import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.MyGraphUtils;
import graph.utilities.cyclefinder.CycleFinder;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 13.03.14
 */
public class CycleFinderTest {
   @Test
   public void testPathFinderor() {
      Graph<MyVertex, MyEdge> g = MyGraphUtils.createSimpleTestGraph();
      graph.utilities.cyclefinder.CycleFinder p = new CycleFinder<>(g);
      assertTrue(p.findCyclesAndPaths().size() > 0);
   }

}
