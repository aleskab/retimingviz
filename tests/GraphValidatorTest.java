import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyEdgeFactory;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import graph.utilities.MyGraphUtils;
import graph.utilities.graphvalidator.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 5.03.14
 */
public class GraphValidatorTest {
   private MyVertex cVertex;
   private MyVertex zffVertex;

   private Graph<MyVertex, MyEdge> getCyclicGraph() {
      Graph<MyVertex, MyEdge> g = new DirectedSparseMultigraph<>();
      MyEdgeFactory edgeFactory = MyEdgeFactory.getInstance();
      MyVertexFactory vertexFactory = MyVertexFactory.getInstance();
      cVertex = vertexFactory.create("C", 0);
      zffVertex = vertexFactory.create("ZFF", 4);
      g.addVertex(cVertex);
      g.addVertex(vertexFactory.create("E", 1));
      g.addVertex(vertexFactory.create("F", 2));
      g.addVertex(vertexFactory.create("G", 3));
      g.addVertex(zffVertex);
      ArrayList<MyVertex> vertexArrayList = new ArrayList<>(g.getVertices());
      Collections.sort(vertexArrayList, new Comparator<MyVertex>() {
         public int compare(MyVertex a, MyVertex b) {
            return a.getName().compareTo(b.getName());
         }
      });
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(0), vertexArrayList.get(1));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(1), vertexArrayList.get(2));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(1), vertexArrayList.get(3));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(4), vertexArrayList.get(3));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(3), vertexArrayList.get(2));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(2), vertexArrayList.get(0));

      return g;
   }

   private Graph<MyVertex, MyEdge> getNotCyclicInvalidGraph() {
      Graph<MyVertex, MyEdge> g = getCyclicGraph();
      for (MyVertex v : g.getVertices()) {
         if ("C".equals(v.getName())) {
            g.removeEdge(g.getOutEdges(v).iterator().next());
         }
      }
      return g;
   }

   private Graph<MyVertex, MyEdge> getNotCyclicValidGraph() {
      Graph<MyVertex, MyEdge> g = getCyclicGraph();
      MyEdge edge = g.getInEdges(cVertex).iterator().next();
      edge.addOneRegister();
      g.addEdge(MyEdgeFactory.getInstance().create(1), cVertex, zffVertex);
      return g;
   }

   @Test(expected = CyclicException.class)
   public void validateCyclicGraphTest() throws Exception {
      Graph graphToTest = getCyclicGraph();
      GraphValidator.validate(graphToTest);
   }

   @Test
   public void graphTestNotCyclicValidGraph() throws Exception {
      Graph graphToTest = getNotCyclicValidGraph();
      GraphValidator.validate(graphToTest);
   }

   @Test(expected = GraphValidationException.class)
   public void graphTestNotCyclicWithSource() throws Exception {
      Graph g = getNotCyclicInvalidGraph();
      GraphValidator.validate(g);
   }

   @Test
   public void graphIsCyclicTestExample() throws Exception {
      Graph graphToTest = MyGraphUtils.createGraphFromExercise();
      GraphValidator.validate(graphToTest);
   }

   @Test
   public void graphIsCyclicTestSimple() throws Exception {
      Graph graphToTest = MyGraphUtils.createSimpleTestGraph();
      GraphValidator.validate(graphToTest);
   }

   @Test(expected = ConnectedComponentException.class)
   public void graphIsNotIndependentTest() throws Exception {
      Graph<MyVertex, MyEdge> graphToTest = getCyclicGraph();
      graphToTest.removeEdge(graphToTest.findEdge(zffVertex, graphToTest.getSuccessors(zffVertex).iterator().next()));

      MyVertex newV = MyVertexFactory.getInstance().create("NEW", 1);
      graphToTest.addVertex(newV);
      graphToTest.addEdge(MyEdgeFactory.getInstance().create(1), zffVertex, newV);
      graphToTest.addEdge(MyEdgeFactory.getInstance().create(1), newV, zffVertex);

      GraphValidator.validate(graphToTest);
   }
}
