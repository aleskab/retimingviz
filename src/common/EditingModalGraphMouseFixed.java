package common;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Changed with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.11.13
 */
import common.fixedplugins.*;
import edu.uci.ics.jung.visualization.MultiLayerTransformer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.annotations.AnnotatingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.*;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.MyGraphUtils;
import org.apache.commons.collections15.Factory;

import javax.swing.*;
import javax.swing.plaf.basic.BasicIconFactory;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;

public class EditingModalGraphMouseFixed<V, E> extends AbstractModalGraphMouse implements ModalGraphMouse, ItemSelectable {

   protected Factory<V> vertexFactory;
   protected Factory<E> edgeFactory;
   protected EdgeEditingPlugin<V, E> edgeEditingPlugin;
   protected LabelEditingGraphMousePlugin<V, E> labelEditingPlugin;
   protected EditingPluginPopupFixed<V, E> popupEditPluginFixed;
   protected AnnotatingGraphMousePlugin<V, E> annotatingPlugin;
   protected MultiLayerTransformer basicTransformer;
   protected RenderContext<V, E> rc;

   /**
    * create an instance with default values
    */
   public EditingModalGraphMouseFixed(RenderContext<V, E> rc,
                                      Factory<V> vertexFactory, Factory<E> edgeFactory) {
      this(rc, vertexFactory, edgeFactory, 1.1f, 1 / 1.1f);
   }

   /**
    * create an instance with passed values
    *
    * @param in  override value for scale in
    * @param out override value for scale out
    */
   public EditingModalGraphMouseFixed(RenderContext<V, E> rc,
                                      Factory<V> vertexFactory, Factory<E> edgeFactory, float in, float out) {
      super(in, out);
      this.vertexFactory = vertexFactory;
      this.edgeFactory = edgeFactory;
      this.rc = rc;
      this.basicTransformer = rc.getMultiLayerTransformer();
      loadPlugins();
      setModeKeyListener(new ModeKeyAdapter(this));
   }

   /**
    * create the plugins, and load the plugins for TRANSFORMING mode
    */
   @Override
   protected void loadPlugins() {
      pickingPlugin = new PickingGraphMousePluginFixed<>();
      animatedPickingPlugin = new AnimatedPickingGraphMousePluginFixed(InputEvent.BUTTON1_MASK | InputEvent.ALT_MASK);
      translatingPlugin = new TranslatingGraphMousePluginFixed(InputEvent.BUTTON1_MASK);
      scalingPlugin = new ScalingGraphMousePlugin(new CrossoverScalingControl(), 0, in, out);
      rotatingPlugin = new RotatingGraphMousePlugin();
      shearingPlugin = new ShearingGraphMousePlugin();
      edgeEditingPlugin = new EdgeEditingPlugin<>(vertexFactory, edgeFactory);
      labelEditingPlugin = new LabelEditingGraphMousePlugin<>();

      annotatingPlugin = new AnnotatingGraphMousePlugin<>(rc);
      annotatingPlugin.setFill(false);
      annotatingPlugin.setAnnotationColor(Color.MAGENTA.darker());
      annotatingPlugin.setRectangularShape(new RoundRectangle2D.Double(0, 0, 0, 0, 50, 50));

      popupEditPluginFixed = new EditingPluginPopupFixed<>(vertexFactory, edgeFactory, this);
      add(scalingPlugin);
      setMode(Mode.PICKING);
   }

   /**
    * setter for the Mode.
    */
   @Override
   public void setMode(Mode mode) {
      if (this.mode != mode) {
         fireItemStateChanged(new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, this.mode, ItemEvent.DESELECTED));
         this.mode = mode;

         if (mode != Mode.PICKING) { setEdgeEditEnabled(false); } //disable checkbox in menu

         if (mode == Mode.TRANSFORMING) {
            setTransformingMode();
         } else if (mode == Mode.PICKING) {
            setPickingMode();
         } else if (mode == Mode.ANNOTATING) {
            setAnnotatingMode();
         }
         if (modeBox != null) {
            modeBox.setSelectedItem(ModeCustom.valueOf(mode));
         }
         fireItemStateChanged(new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, mode, ItemEvent.SELECTED));
      }
   }

   @Override
   protected void setPickingMode() {
      popupEditPluginFixed.init();
      remove(translatingPlugin);
      remove(rotatingPlugin);
      remove(shearingPlugin);
      remove(annotatingPlugin);
      add(pickingPlugin);
      add(animatedPickingPlugin);
      add(labelEditingPlugin);
      add(popupEditPluginFixed);
   }

   @Override
   protected void setTransformingMode() {
      remove(pickingPlugin);
      remove(annotatingPlugin);
      remove(popupEditPluginFixed);
      add(animatedPickingPlugin);
      add(translatingPlugin);
      add(rotatingPlugin);
      add(shearingPlugin);
      add(labelEditingPlugin);
   }

   protected void setAnnotatingMode() {
      remove(pickingPlugin);
      remove(animatedPickingPlugin);
      remove(translatingPlugin);
      remove(rotatingPlugin);
      remove(shearingPlugin);
      remove(labelEditingPlugin);
      remove(popupEditPluginFixed);
      add(annotatingPlugin);
   }

   @Override
   public ItemListener getModeListener() {
      if (modeListener == null) {
         modeListener = new ModeListener();
      }
      return modeListener;
   }

   /**
    * @return the modeBox.
    */
   @Override
   public JComboBox getModeComboBox() {
      if (modeBox == null) {
         modeBox = new JComboBox<>(new ModeCustom[]{ModeCustom.TRANSFORMING, ModeCustom.PICKING, ModeCustom.ANNOTATING});
         modeBox.addItemListener(getModeListener());
      }
      modeBox.setSelectedItem(ModeCustom.valueOf(mode));
      modeBox.setFocusable(false);
      modeBox.setSize(100, 50);
      return modeBox;
   }

   /**
    * create (if necessary) and return a menu that will change
    * the mode
    * @return the menu
    */
   @Override
   public JMenu getModeMenu() {
      if (modeMenu == null) {
         modeMenu = new JMenu();
         Icon icon = BasicIconFactory.getMenuArrowIcon();
         modeMenu.setIcon(BasicIconFactory.getMenuArrowIcon());
         modeMenu.setPreferredSize(new Dimension(icon.getIconWidth() + 10,
                                                        icon.getIconHeight() + 10));

         final JRadioButtonMenuItem transformingButton = new JRadioButtonMenuItem("Transform");
         transformingButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
               if (e.getStateChange() == ItemEvent.SELECTED) {
                  setMode(Mode.TRANSFORMING);
               }
            }
         });

         final JRadioButtonMenuItem pickingButton = new JRadioButtonMenuItem("Pick / Edit");
         pickingButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
               if (e.getStateChange() == ItemEvent.SELECTED) {
                  setMode(Mode.PICKING);
               }
            }
         });

         final JRadioButtonMenuItem annotatingButton = new JRadioButtonMenuItem("Annotate");
         annotatingButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
               if (e.getStateChange() == ItemEvent.SELECTED) {
                  setMode(Mode.ANNOTATING);
               }
            }
         });

         final JMenuItem aboutButton = new JMenuItem("About");
         aboutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               JOptionPane.showOptionDialog(modeMenu
                                                   , "Retiming algorithm visualization tool \n" +
                                                     "\nCreated by: Oleg Bahvalov" +
                                                     "\nVersion: 0.3.7" +
                                                     "\nMay 2014" +
                                                     "\n\nLicensed under New BSD License(BSD 3-Clause)"
                                                   , "RetimingViz info",
                                                   JOptionPane.DEFAULT_OPTION,
                                                   JOptionPane.PLAIN_MESSAGE,
                                                   null,     //do not use a custom Icon
                                                   null,  //the titles of buttons
                                                   null); //default button title
            }
         });

         final JMenuItem exitButton = new JMenuItem("Exit");
         exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               System.exit(0);
            }
         });

         ButtonGroup radio = new ButtonGroup();
         radio.add(transformingButton);
         radio.add(pickingButton);
         radio.add(annotatingButton);
         transformingButton.setSelected(true);
         modeMenu.add(transformingButton);
         modeMenu.add(pickingButton);
         modeMenu.add(annotatingButton);
         modeMenu.addSeparator();
         modeMenu.add(aboutButton);
         modeMenu.add(exitButton);

         modeMenu.setToolTipText("Menu for setting Mouse Mode");
         addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
               if (e.getStateChange() == ItemEvent.SELECTED) {
                  if (e.getItem() == Mode.TRANSFORMING) {
                     transformingButton.setSelected(true);
                  } else if (e.getItem() == Mode.PICKING) {
                     pickingButton.setSelected(true);
                  } else if (e.getItem() == Mode.ANNOTATING) {
                     annotatingButton.setSelected(true);
                  }
               }
            }
         });
      }
      return modeMenu;
   }

   public static class ModeKeyAdapter extends KeyAdapter {
      private char t = 't';
      private char p = 'p';
      private char a = 'a';
      protected ModalGraphMouse graphMouse;

      public ModeKeyAdapter(ModalGraphMouse graphMouse) {
         this.graphMouse = graphMouse;
      }

      @Override
      public void keyTyped(KeyEvent event) {
         char keyChar = event.getKeyChar();
         if (keyChar == t) {
            ((Component) event.getSource()).setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            graphMouse.setMode(Mode.TRANSFORMING);
         } else if (keyChar == p) {
            ((Component) event.getSource()).setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            graphMouse.setMode(Mode.PICKING);
         } else if (keyChar == a) {
            ((Component) event.getSource()).setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            graphMouse.setMode(Mode.ANNOTATING);
         }
      }
   }

   class ModeListener implements ItemListener {
      public void itemStateChanged(ItemEvent e) {
         ModeCustom selected = (ModeCustom) e.getItem();
         setMode(selected.getMode());
      }
   }

   public void setEdgeEditEnabled(boolean enable) {
      if (enable) {
         remove(pickingPlugin);
         remove(animatedPickingPlugin);
         add(edgeEditingPlugin);
      } else {
         remove(edgeEditingPlugin);
         add(animatedPickingPlugin);
         add(pickingPlugin);
      }
   }

   public void centerGraphLocation(VisualizationViewer<MyVertex, MyEdge> vv) {
      Point2D center = MyGraphUtils.getCenterPointForLayout(vv.getGraphLayout());
      MouseEvent event = new MouseEvent(vv, 1488, System.currentTimeMillis(), InputEvent.BUTTON1_MASK | InputEvent.ALT_MASK, (int) center.getX(), (int) center.getY(), 1, false);
      mouseClicked(event);
   }

   private enum ModeCustom {
      /**
       * Enum with modified toString method
       * getMode() returns original analog Mode
       */
      TRANSFORMING("Transform", Mode.TRANSFORMING), ANNOTATING("Annotate", Mode.ANNOTATING), PICKING("Pick / Edit", Mode.PICKING);
      private String label;
      private Mode mode;

      private ModeCustom(String label, Mode mode) {
         this.label = label;
         this.mode = mode;
      }

      Mode getMode() {
         return mode;
      }

      static ModeCustom valueOf(Mode mode) {
         switch (mode) {
            case ANNOTATING:
               return ANNOTATING;
            case PICKING:
               return PICKING;
            default:
               return TRANSFORMING;
         }
      }

      @Override
      public String toString() {
         return label;
      }
   }
}


