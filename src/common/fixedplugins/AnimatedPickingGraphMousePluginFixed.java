package common.fixedplugins;

import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AnimatedPickingGraphMousePlugin;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 21.02.14
 */
public class AnimatedPickingGraphMousePluginFixed<V, E> extends AnimatedPickingGraphMousePlugin<V, E> {
   @Override
   public void mouseClicked(MouseEvent e) {
      centerGraph(e);
   }

   public void centerGraph(MouseEvent e) {
      //noinspection MagicConstant
      if (e.getModifiers() == modifiers) {
         @SuppressWarnings("unchecked") final VisualizationViewer<V, E> vv = (VisualizationViewer<V, E>) e.getSource();
         Point2D q = e.getPoint();
         Point2D lvc = vv.getRenderContext().getMultiLayerTransformer().inverseTransform(vv.getCenter());
         final double dx = (lvc.getX() - q.getX()) / 10;
         final double dy = (lvc.getY() - q.getY()) / 10;
         Runnable animator = new Runnable() {
            public void run() {
               for (int i = 0; i < 10; i++) {
                  vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).translate(dx, dy);
                  try {
                     Thread.sleep(100);
                  } catch (InterruptedException ex) {
                     System.out.println("Animation thread interrupted.");
                  }
               }
            }
         };
         Thread thread = new Thread(animator);
         thread.start();
      }
   }

   public AnimatedPickingGraphMousePluginFixed(int selectionModifiers) {
      super(selectionModifiers);
   }
}
