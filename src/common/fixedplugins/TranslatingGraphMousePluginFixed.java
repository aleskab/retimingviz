package common.fixedplugins;

import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.TranslatingGraphMousePlugin;
import edu.uci.ics.jung.visualization.transform.MutableTransformer;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 20.02.14
 */
public class TranslatingGraphMousePluginFixed extends TranslatingGraphMousePlugin {

   public TranslatingGraphMousePluginFixed(int modifiers) {
      super(modifiers);
   }

   private boolean checkMousePosition(int x, int y, Dimension d) {
      //graph still can be moved outside of frame
      return x < d.getWidth() && y < d.getHeight() && x > 0 && y > 0;
   }

   @Override
   public void mouseDragged(MouseEvent e) {
      VisualizationViewer vv = (VisualizationViewer) e.getSource();
      boolean accepted = checkModifiers(e) && checkMousePosition(e.getX(), e.getY(), vv.getSize());
      if (accepted) {
         MutableTransformer modelTransformer = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
         vv.setCursor(cursor);
         try {
            Point2D q = vv.getRenderContext().getMultiLayerTransformer().inverseTransform(down);
            Point2D p = vv.getRenderContext().getMultiLayerTransformer().inverseTransform(e.getPoint());
            float dx = (float) (p.getX() - q.getX());
            float dy = (float) (p.getY() - q.getY());

            modelTransformer.translate(dx, dy);
            down.x = e.getX();
            down.y = e.getY();
         } catch (RuntimeException ex) {
            System.err.println("down = " + down + ", e = " + e);
            throw ex;
         }

         e.consume();
         vv.repaint();
      }
   }
}
