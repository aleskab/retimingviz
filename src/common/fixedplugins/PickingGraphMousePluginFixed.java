package common.fixedplugins;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.PickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.picking.PickedState;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 18.02.14
 * Created to catch an exception, that is produced when you drag a mouse with opened edit popupmenu
 */
public class PickingGraphMousePluginFixed<V, E> extends PickingGraphMousePlugin {

   @Override
   public void mouseDragged(MouseEvent e) {
      if (!locked) {
         @SuppressWarnings("unchecked")
         VisualizationViewer<V, E> vv = (VisualizationViewer<V, E>) e.getSource();
         if (vertex != null) {
            Point p = e.getPoint();
            Point2D graphPoint = vv.getRenderContext().getMultiLayerTransformer().inverseTransform(p);
            Point2D graphDown = vv.getRenderContext().getMultiLayerTransformer().inverseTransform(down);
            Layout<V, E> layout = vv.getGraphLayout();
            double dx = graphPoint.getX() - graphDown.getX();
            double dy = graphPoint.getY() - graphDown.getY();
            PickedState<V> ps = vv.getPickedVertexState();

            for (V v : ps.getPicked()) {
               Point2D vp = layout.transform(v);
               vp.setLocation(vp.getX() + dx, vp.getY() + dy);
               layout.setLocation(v, vp);
            }
            down = p;

         } else {
            Point2D out = e.getPoint();
            //noinspection MagicConstant
            if (e.getModifiers() == this.addToSelectionModifiers ||
                        e.getModifiers() == modifiers) {
               try {
                  rect.setFrameFromDiagonal(down, out);
               } catch (NullPointerException ignored) {
               }  //Catching exception in AWT-eventqueue.
            }
         }
         if (vertex != null) e.consume();
         vv.repaint();
      }
   }
}
