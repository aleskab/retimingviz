package common.fixedplugins;

import common.EditingModalGraphMouseFixed;
import edu.uci.ics.jung.algorithms.layout.GraphElementAccessor;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractPopupGraphMousePlugin;
import edu.uci.ics.jung.visualization.picking.PickedState;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import graph.utilities.MyGraphUtils;
import org.apache.commons.collections15.Factory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.logging.Logger;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Refactored with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.02.14
 * a plugin that uses popup menus to create vertices and directed edges.
 */
public class EditingPluginPopupFixed<V, E> extends AbstractPopupGraphMousePlugin {
   private static final Logger LOG = Logger.getLogger(EditingPluginPopupFixed.class.getName());

   protected Factory<V> vertexFactory;
   protected Factory<E> edgeFactory;
   protected JPopupMenu popup = new JPopupMenu();
   private boolean isEdgeEditEnabled = false;
   private final EditingModalGraphMouseFixed mouseMode;

   public EditingPluginPopupFixed(Factory<V> vertexFactory, Factory<E> edgeFactory, EditingModalGraphMouseFixed emg) {
      this.vertexFactory = vertexFactory;
      this.edgeFactory = edgeFactory;
      this.mouseMode = emg;
   }

   protected void handlePopup(MouseEvent e) {
      popup.removeAll(); //BUGFIX

      @SuppressWarnings("unchecked")
      final VisualizationViewer<V, E> vv = (VisualizationViewer<V, E>) e.getSource();
      final Layout<V, E> layout = vv.getGraphLayout();
      final Graph<V, E> graph = layout.getGraph();
      final Point2D ivp = e.getPoint();
      GraphElementAccessor<V, E> pickSupport = vv.getPickSupport();
      if (pickSupport != null) {

         final V vertex = pickSupport.getVertex(layout, ivp.getX(), ivp.getY());  //selected vertex (at point)
         final E edge = pickSupport.getEdge(layout, ivp.getX(), ivp.getY());
         final PickedState<V> pickedVertexState = vv.getPickedVertexState();
         final PickedState<E> pickedEdgeState = vv.getPickedEdgeState();

         JCheckBox toggle = new JCheckBox("Editing with mouse") {{
            setSelected(isEdgeEditEnabled);
            setFont(UIManager.getFont("CheckBox.font"));
         }};
         toggle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
               isEdgeEditEnabled = (e.getStateChange() == ItemEvent.SELECTED);
               mouseMode.setEdgeEditEnabled(isEdgeEditEnabled);
            }
         });
         popup.add(toggle);

         if (vertex != null) {
            JMenu directedMenu = new JMenu("Create Directed Edge") {{
               setFont(UIManager.getFont("Menu.font"));
            }};
            popup.add(directedMenu);
            for (final V other : graph.getVertices()) {
               if (MyGraphUtils.allowedToCreateEdge(vertex, other, graph))
                  directedMenu.add(new AbstractAction("[" + vertex + "," + other + "]") {
                     public void actionPerformed(ActionEvent e) {
                        E edge = edgeFactory.create();
                        if (edge != null) {
                           graph.addEdge(edge, vertex, other, EdgeType.DIRECTED);
                        }
                        vv.repaint();
                     }
                  });
            }
            if (directedMenu.getItemCount() == 0) popup.remove(directedMenu);
            popup.add(new AbstractAction("Delete Vertex") {
               public void actionPerformed(ActionEvent e) {
                  pickedVertexState.pick(vertex, false);
                  MyVertexFactory.removeName(((MyVertex) vertex).getName());
                  graph.removeVertex(vertex);
                  vv.repaint();
               }
            });
            popup.add(new AbstractAction("Change Name of Vertex") {
               public void actionPerformed(ActionEvent e) {
                  pickedVertexState.pick(vertex, false);
                  ((MyVertex) vertex).setName(JOptionPane.showInputDialog("Enter new name:"));
                  vv.repaint();
               }
            });
            popup.add(new AbstractAction("Change propagation delay of Vertex") {
               public void actionPerformed(ActionEvent e) {
                  pickedVertexState.pick(vertex, false);
                  try {
                     ((MyVertex) vertex).setPropagationDelay(Integer.parseInt(JOptionPane.showInputDialog("Enter new delay:")));
                  } catch (Exception ex) {
                     LOG.severe("Could not change delay for vertex " + vertex.toString() + ": " + ex.getMessage());
                     showWarningDialog(ex.getMessage(), "Delay format error!");
                  }
                  vv.repaint();
               }
            });

         } else if (edge != null) {
            popup.add(new AbstractAction("Delete Edge") {
               public void actionPerformed(ActionEvent e) {
                  pickedEdgeState.pick(edge, false);
                  graph.removeEdge(edge);
                  vv.repaint();
               }
            });
            popup.add(new AbstractAction("Remove One Register") {
               public void actionPerformed(ActionEvent e) {
                  pickedEdgeState.pick(edge, false);
                  ((MyEdge) edge).removeOneRegister();
                  vv.repaint();
               }
            });
            popup.add(new AbstractAction("Add One Register") {
               public void actionPerformed(ActionEvent e) {
                  pickedEdgeState.pick(edge, false);
                  ((MyEdge) edge).addOneRegister();
                  vv.repaint();
               }
            });
            popup.add(new AbstractAction("Remove All Registers") {
               public void actionPerformed(ActionEvent e) {
                  pickedEdgeState.pick(edge, false);
                  ((MyEdge) edge).removeAllRegisters();
                  vv.repaint();
               }
            });
         } else {
            popup.add(new AbstractAction("Create Vertex") {
               public void actionPerformed(ActionEvent e) {
                  V newVertex = vertexFactory.create();
                  if (newVertex != null) {
                     graph.addVertex(newVertex);
                     layout.setLocation(newVertex, vv.getRenderContext().getMultiLayerTransformer().inverseTransform(ivp));
                     vv.repaint();
                  }
               }
            });
         }
         if (popup.getComponentCount() > 0) {
            popup.show(vv, e.getX(), e.getY());
         }
      }
   }

   private void showWarningDialog(String message, String title) {
      JOptionPane.showMessageDialog(null, message, title, JOptionPane.WARNING_MESSAGE);
   }

   public void init() {
      isEdgeEditEnabled = false;
   }
}


