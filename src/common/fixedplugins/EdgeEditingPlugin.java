package common.fixedplugins;

import edu.uci.ics.jung.algorithms.layout.GraphElementAccessor;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractGraphMousePlugin;
import edu.uci.ics.jung.visualization.util.ArrowFactory;
import graph.utilities.MyGraphUtils;
import org.apache.commons.collections15.Factory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Refactored with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 09.04.14
 * A changed version of EditingGraphMousePlugin.java from JUNG library with deleted edgeIsDirected field and EdgeType checks.
 */
public class EdgeEditingPlugin<V, E> extends AbstractGraphMousePlugin implements
        MouseListener, MouseMotionListener {

   protected V startVertex;
   protected Point2D down;

   protected CubicCurve2D rawEdge = new CubicCurve2D.Float();
   protected Shape edgeShape;
   protected Shape rawArrowShape;
   protected Shape arrowShape;
   protected VisualizationServer.Paintable edgePaintable;
   protected VisualizationServer.Paintable arrowPaintable;
   protected Factory<V> vertexFactory;
   protected Factory<E> edgeFactory;

   public EdgeEditingPlugin(Factory<V> vertexFactory, Factory<E> edgeFactory) {
      this(MouseEvent.BUTTON1_MASK, vertexFactory, edgeFactory);
   }

   /**
    * create instance and prepare shapes for visual effects
    *
    * @param modifiers input modifiers
    */
   public EdgeEditingPlugin(int modifiers, Factory<V> vertexFactory, Factory<E> edgeFactory) {
      super(modifiers);
      this.vertexFactory = vertexFactory;
      this.edgeFactory = edgeFactory;
      rawEdge.setCurve(0.0f, 0.0f, 0.33f, 100, .66f, -50,
                              1.0f, 0.0f);
      rawArrowShape = ArrowFactory.getNotchedArrow(20, 16, 8);
      edgePaintable = new EdgePaintable();
      arrowPaintable = new ArrowPaintable();
      this.cursor = Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
   }

   /**
    * Overridden to be more flexible, and pass events with
    * key combinations. The default responds to both ButtonOne
    * and ButtonOne+Shift
    */
   @Override
   public boolean checkModifiers(MouseEvent e) {
      return (e.getModifiers() & modifiers) != 0;
   }

   /**
    * If the mouse is pressed in an empty area, create a new vertex there.
    * If the mouse is pressed on an existing vertex, prepare to create
    * an edge from that vertex to another
    */
   public void mousePressed(MouseEvent e) {
      if (checkModifiers(e)) {
         final VisualizationViewer<V, E> vv = getViewerFromEventObject(e.getSource());
         final Point2D p = e.getPoint();
         GraphElementAccessor<V, E> pickSupport = vv.getPickSupport();
         if (pickSupport != null) {
            Graph<V, E> graph = vv.getModel().getGraphLayout().getGraph();

            final V vertex = pickSupport.getVertex(vv.getModel().getGraphLayout(), p.getX(), p.getY());
            if (vertex != null) { // get ready to make an edge
               startVertex = vertex;
               down = e.getPoint();
               transformEdgeShape(down, down);
               vv.addPostRenderPaintable(edgePaintable);
               transformArrowShape(down, e.getPoint());
               vv.addPostRenderPaintable(arrowPaintable);
            } else { // make a new vertex

               V newVertex = vertexFactory.create();
               if (newVertex != null) {
                  Layout<V, E> layout = vv.getModel().getGraphLayout();
                  graph.addVertex(newVertex);
                  layout.setLocation(newVertex, vv.getRenderContext().getMultiLayerTransformer().inverseTransform(e.getPoint()));
               }
            }
         }
         vv.repaint();
      }
   }

   /**
    * If startVertex is non-null, and the mouse is released over an
    * existing vertex, create an undirected edge from startVertex to
    * the vertex under the mouse pointer. If shift was also pressed,
    * create a directed edge instead.
    */
   public void mouseReleased(MouseEvent e) {
      if (checkModifiers(e)) {
         final VisualizationViewer<V, E> vv = getViewerFromEventObject(e.getSource());
         final Point2D p = e.getPoint();
         Layout<V, E> layout = vv.getModel().getGraphLayout();
         GraphElementAccessor<V, E> pickSupport = vv.getPickSupport();
         if (pickSupport != null) {
            final V vertex = pickSupport.getVertex(layout, p.getX(), p.getY());
            if (vertex != null && startVertex != null) {
               Graph<V, E> graph =
                       vv.getGraphLayout().getGraph();
               if (MyGraphUtils.allowedToCreateEdge(startVertex, vertex, graph)) {
                  E edge = edgeFactory.create();
                  if (edge != null) {
                     graph.addEdge(edge, startVertex, vertex, EdgeType.DIRECTED);
                  }
               }
               vv.repaint();
            }
         }
         startVertex = null;
         down = null;
         vv.removePostRenderPaintable(edgePaintable);
         vv.removePostRenderPaintable(arrowPaintable);
      }
   }

   /**
    * If startVertex is non-null, stretch an edge shape between
    * startVertex and the mouse pointer to simulate edge creation
    */
   public void mouseDragged(MouseEvent e) {
      if (checkModifiers(e)) {
         if (startVertex != null) {
            transformEdgeShape(down, e.getPoint());
            transformArrowShape(down, e.getPoint());
         }
         VisualizationViewer<V, E> vv = getViewerFromEventObject(e.getSource());
         vv.repaint();
      }
   }

   /**
    * code lifted from PluggableRenderer to move an edge shape into an
    * arbitrary position
    */
   private void transformEdgeShape(Point2D down, Point2D out) {
      float x1 = (float) down.getX();
      float y1 = (float) down.getY();
      float x2 = (float) out.getX();
      float y2 = (float) out.getY();

      AffineTransform xform = AffineTransform.getTranslateInstance(x1, y1);

      float dx = x2 - x1;
      float dy = y2 - y1;
      float thetaRadians = (float) Math.atan2(dy, dx);
      xform.rotate(thetaRadians);
      float dist = (float) Math.sqrt(dx * dx + dy * dy);
      xform.scale(dist / rawEdge.getBounds().getWidth(), 1.0);
      edgeShape = xform.createTransformedShape(rawEdge);
   }

   private void transformArrowShape(Point2D down, Point2D out) {
      float x1 = (float) down.getX();
      float y1 = (float) down.getY();
      float x2 = (float) out.getX();
      float y2 = (float) out.getY();

      AffineTransform xform = AffineTransform.getTranslateInstance(x2, y2);

      float dx = x2 - x1;
      float dy = y2 - y1;
      float thetaRadians = (float) Math.atan2(dy, dx);
      xform.rotate(thetaRadians);
      arrowShape = xform.createTransformedShape(rawArrowShape);
   }

   /**
    * Used for the edge creation visual effect during mouse drag
    */
   class EdgePaintable implements VisualizationServer.Paintable {

      public void paint(Graphics g) {
         if (edgeShape != null) {
            Color oldColor = g.getColor();
            g.setColor(Color.black);
            ((Graphics2D) g).draw(edgeShape);
            g.setColor(oldColor);
         }
      }

      public boolean useTransform() {
         return false;
      }
   }

   /**
    * Used for the directed edge creation visual effect during mouse drag
    */
   class ArrowPaintable implements VisualizationServer.Paintable {

      public void paint(Graphics g) {
         if (arrowShape != null) {
            Color oldColor = g.getColor();
            g.setColor(Color.black);
            ((Graphics2D) g).fill(arrowShape);
            g.setColor(oldColor);
         }
      }

      public boolean useTransform() {
         return false;
      }
   }

   public void mouseClicked(MouseEvent e) {
   }

   public void mouseEntered(MouseEvent e) {
      JComponent c = (JComponent) e.getSource();
      c.setCursor(cursor);
   }

   public void mouseExited(MouseEvent e) {
      JComponent c = (JComponent) e.getSource();
      c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
   }

   public void mouseMoved(MouseEvent e) {
   }

   @SuppressWarnings("unchecked")
   private VisualizationViewer<V, E> getViewerFromEventObject(Object source) {
      if (source instanceof VisualizationViewer) {
         return (VisualizationViewer<V, E>)source;
      }
      return null;
   }
}
