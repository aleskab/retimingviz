package launcher;

import javax.swing.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 12.05.14
 */
public class AppletLauncher extends JApplet {
   @Override
   public void start() {
      AppLauncher.main(new String[0]);
   }
}
