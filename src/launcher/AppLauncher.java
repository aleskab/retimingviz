package launcher;

import ui.MainSplitPane;
import ui.parts.RetiMenuBar;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 25.10.13
 */
public class AppLauncher {

   public static void main(String[] args) {
      setLookAndFeel();
      showMainWindow();
   }

   private static void showMainWindow() {
      JFrame frame = new JFrame("RetimingViz 0.3.7");
      frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      frame.setLayout(new BorderLayout());

      MainSplitPane mainSplitPane = new MainSplitPane(frame);
      frame.add(mainSplitPane);
      frame.setJMenuBar(new RetiMenuBar(mainSplitPane));

      frame.pack();
      frame.setVisible(true);
   }

   private static void setLookAndFeel() {
      try {
         for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
               UIManager.setLookAndFeel(info.getClassName());
               break;
            }
         }

         Map<Object, Object> lookAndFeelDefaults = new HashMap<>(UIManager.getLookAndFeelDefaults());
         for (Object key : lookAndFeelDefaults.keySet()) {
            if (!key.toString().endsWith(".font")) continue;
            Font font = UIManager.getFont(key);
            Font newFont = font.deriveFont(1.2f * font.getSize2D());
            if (System.getProperty("os.name").toLowerCase().contains("linux"))
               newFont = newFont.deriveFont(Font.PLAIN);
            UIManager.put(key, newFont);
         }
      } catch (Exception e) {
         // If Nimbus is not available, uses defaults
      }
   }
}
