package graph;

import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyVertex;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 29.11.13
 */
public class StepDetails {
   private static StepDetails ourInstance = new StepDetails();

   private StepDetails() { }

   private List<MyVertex> nodeList;
   private Map dataReadinessMap;
   private Graph<MyVertex, MyEdge> clearedGraph;
   private Map<MyVertex, Point2D> vertexLocations;
   private int timeRequirement;
 

   public static StepDetails getInstance() {
      return ourInstance;
   }

   public Map<MyVertex, Point2D> getVertexLocations() {
      return vertexLocations;
   }

   public Graph<MyVertex, MyEdge> getClearedGraph() {
      return clearedGraph;
   }

   public Map getDataReadinessMap() {
      return dataReadinessMap;
   }

   public List<MyVertex> getNodeList() {
      return nodeList;
   }
   
   public int getTimeReq() {
	   return timeRequirement;
   }

   public void setDetails(Graph<MyVertex, MyEdge> clearedGraph, Map<MyVertex, Integer> map, List<MyVertex> nodesToRetime, Map<MyVertex, Point2D> locations, int timeReq) {
      this.clearedGraph = clearedGraph;
      this.dataReadinessMap = map;
      this.nodeList = nodesToRetime;
      this.vertexLocations = locations;
      this.timeRequirement = timeReq;
   }

   public void clearData() {
      setDetails(null, null, null, null, -1);
   }
}
