package graph.utilities.graphgenerator;

import edu.uci.ics.jung.algorithms.generators.GraphGenerator;
import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyEdgeFactory;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import org.apache.commons.collections15.Factory;
import org.apache.commons.lang.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 11.03.14
 * Customized default GraphGenerator from JUNG2 library
 */
public class GraphCreator implements GraphGenerator {
   private int mNumVertices;
   private int mNumEdges;
   private Random mRandom;
   private Factory<Graph<MyVertex, MyEdge>> graphFactory;
   private MyVertexFactory vertexFactory;
   private MyEdgeFactory edgeFactory;

   /**
    * Creates an instance with the specified factories and specifications.
    *
    * @param graphFactory  the factory to use to generate the graph
    * @param vertexFactory the factory to use to create vertices
    * @param edgeFactory   the factory to use to create edges
    * @param numVertices   the number of vertices for the generated graph
    * @param numEdges      the number of edges the generated graph will have, should be Theta(numVertices)
    *                      distribution will approximate a power-law
    */
   public GraphCreator(Factory<Graph<MyVertex, MyEdge>> graphFactory,
                       MyVertexFactory vertexFactory, MyEdgeFactory edgeFactory,
                       int numVertices, int numEdges) {
      this.graphFactory = graphFactory;
      this.vertexFactory = vertexFactory;
      this.edgeFactory = edgeFactory;
      mNumVertices = numVertices;
      mNumEdges = numEdges;
      mRandom = new Random();
   }

   private String getRandomName() {
      return RandomStringUtils.randomAlphabetic(3);
   }

   private int getRandomTime() {
      return mRandom.nextInt(20);
   }

   protected Graph<MyVertex, MyEdge> initializeGraph() {
      Graph<MyVertex, MyEdge> graph = graphFactory.create();
      for (int i = 0; i < mNumVertices; i++) {
         graph.addVertex(vertexFactory.create(getRandomName(), getRandomTime()));
      }
      List<MyVertex> vertices = new ArrayList<>(graph.getVertices());
      while (graph.getEdgeCount() < mNumEdges) {
         MyVertex u = vertices.get((int) (mRandom.nextDouble() * mNumVertices));
         MyVertex v = vertices.get((int) (mRandom.nextDouble() * mNumVertices));
         boolean edgeExistsForPair = graph.findEdge(u,v) != null || graph.findEdge(v, u) != null;
         if (!edgeExistsForPair && !v.equals(u)) {
            graph.addEdge(edgeFactory.create(mRandom.nextInt(3)), u, v);
         }
      }
      return graph;
   }

   /**
    * Generates a random graph
    * @return the generated graph
    */
   public Graph<MyVertex, MyEdge> create() {
      return initializeGraph();
   }
}
