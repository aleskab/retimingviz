package graph.utilities.graphgenerator;

import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyEdgeFactory;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import graph.utilities.graphvalidator.GraphValidationException;
import graph.utilities.graphvalidator.GraphValidator;
import org.apache.commons.collections15.Factory;

import javax.swing.*;
import java.util.Random;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 11.03.14
 * This Class successfully generate graphs that have from 3 to 17 vertices.
 * If number of vertices is < 3 then this graph is not valid for retiming, generate() returns null.
 * If value is larger than 17, generation can take a really(!) long time, since it heavily relies on random.
 * Generation continues while success is not achieved or user did not pressed cancel.
 */
public abstract class Generator {
   private static final Factory<Graph<MyVertex, MyEdge>> graphFactory = new Factory<Graph<MyVertex, MyEdge>>() {
      @Override
      public Graph<MyVertex, MyEdge> create() {
         return new DirectedSparseMultigraph<>();
      }
   };
   private static JDialog stopDialog;
   private static boolean finished;
   private static Graph<MyVertex, MyEdge> generatedGraph = null;

   public static Graph<MyVertex, MyEdge> generate(final int vertexCount, final JComponent owner) {
      new Thread(new Runnable() {
         @Override
         public void run() {
            finished = false;
            if (vertexCount < 3) {
               //TODO: generate simple graph
            } else {
               Random rand = new Random();
               int edgesNum;
               while (true) {
                  int val = vertexCount * (vertexCount - 1) / 2;  //n*(n-1)/2 - connected graph edge count
                  edgesNum = rand.nextInt(val + 1);   //from 0 to val
                  if (edgesNum >= vertexCount) {
                     break;
                  }
               }
              generatedGraph = generate(vertexCount, edgesNum);
            }
         }
      }).start();
      showCancelOptionDialog(owner);
      return generatedGraph;
   }

   private static void showCancelOptionDialog(JComponent component) {
      JOptionPane question = new JOptionPane();
      question.setOptions(new Object[]{"Cancel"});
      question.setMessage("To stop generation press Cancel or close that dialog");
      question.setMessageType(JOptionPane.INFORMATION_MESSAGE);
      stopDialog = question.createDialog("Graph generation in progress...  (current tries: 0)");
      stopDialog.setLocationRelativeTo(component);
      if (!finished) { //this check is for case when generation is already completed before this step;
         stopDialog.setVisible(true);
         finished = true; //if was closed then stop generation
      }
   }


   private static Graph<MyVertex, MyEdge> generate(int vertexCount, int edgeCount) {
      GraphCreator creator = new GraphCreator(graphFactory, MyVertexFactory.getInstance(), MyEdgeFactory.getInstance(), vertexCount, edgeCount);
      int step = 0;
      while (!finished) {
         Graph<MyVertex, MyEdge> g = creator.create();
         try {
            GraphValidator.validate(g);
            generatedGraph = g;
            finished = true;
            stopDialog.dispose();
            return g;
         } catch (GraphValidationException e) {
            System.out.println(step++ + " " + e.getMessage());
            if (stopDialog != null) {
               stopDialog.setTitle("Graph generation in progress...  (current tries: " + step + ")");
            }
         } catch (NullPointerException consumed) {/*ignore exception*/}
      }
      return null;
   }
}
