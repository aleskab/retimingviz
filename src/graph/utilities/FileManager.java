package graph.utilities;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.io.GraphMLWriter;
import edu.uci.ics.jung.io.graphml.*;
import graph.elements.MyEdge;
import graph.elements.MyEdgeFactory;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import org.apache.commons.collections15.Transformer;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.03.14
 */
public class FileManager {
   public FileManager() { }

   private static JFileChooser fileChooser = new JFileChooser() {{
      setFileSelectionMode(JFileChooser.FILES_ONLY);
      setFileFilter(new FileNameExtensionFilter("xml files (*.xml)", "xml"));
   }};

   private static final Logger LOG = Logger.getLogger(FileManager.class.getName());
   private static final String XML_EXTENSION = ".xml";
   private static final String X = "x";
   private static final String Y = "y";
   private static final String NAME = "name";
   private static final String DELAY = "delay";
   private static final String DEFAULT_NAME = "default";
   private static final String ZERO = "0";
   private static final String REGISTERS = "registerCount";

   private static Map<MyVertex, Point2D> vertexLocations = new HashMap<>();

   private static GraphMLWriter<MyVertex, MyEdge> getGraphWriter(final Map<MyVertex, Point2D> vertexLocations) {
      GraphMLWriter<MyVertex, MyEdge> graphWriter = new GraphMLWriter<>();


      graphWriter.addVertexData(X, null, ZERO, new Transformer<MyVertex, String>() {
         public String transform(MyVertex v) {
            return Double.toString(vertexLocations.get(v).getX());
         }
      });

      graphWriter.addVertexData(Y, null, ZERO, new Transformer<MyVertex, String>() {
         public String transform(MyVertex v) {
            return Double.toString(vertexLocations.get(v).getY());
         }
      });

      graphWriter.addVertexData(NAME, null, DEFAULT_NAME, new Transformer<MyVertex, String>() {
         public String transform(MyVertex v) {
            return v.getName();
         }
      });

      graphWriter.addVertexData(DELAY, null, ZERO, new Transformer<MyVertex, String>() {
         public String transform(MyVertex v) {
            return Integer.toString(v.getPropagationDelay());
         }
      });

      graphWriter.addEdgeData(REGISTERS, null, ZERO, new Transformer<MyEdge, String>() {
         @Override
         public String transform(MyEdge myEdge) {
            return Integer.toString(myEdge.getRegisterCount());
         }
      });

      return graphWriter;
   }

   public static void saveState(final Layout<MyVertex, MyEdge> l) {
      final Map<MyVertex, Point2D> map = MyGraphUtils.getVertexLocationsForLayout(l);
      GraphMLWriter<MyVertex, MyEdge> graphWriter = getGraphWriter(map);

      if (showFileDialog(JFileChooser.SAVE_DIALOG) == JFileChooser.APPROVE_OPTION) {
         try {
            String filename = fileChooser.getSelectedFile().toString().concat(XML_EXTENSION);
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
            graphWriter.save(l.getGraph(), out);
            LOG.info("Graph data successfully saved to file: " + filename);
         } catch (Exception e) {
            LOG.severe("Error while writing graph data to file, cause: " + e.getMessage());
         }
      }
   }

   public static Layout<MyVertex, MyEdge> loadState() {
      if (showFileDialog(JFileChooser.OPEN_DIALOG) == JFileChooser.APPROVE_OPTION) {
         try {
            String filename = fileChooser.getSelectedFile().toString();
            if (!filename.endsWith(XML_EXTENSION)) {
               filename = filename.concat(XML_EXTENSION);
            }

            BufferedReader fileReader = new BufferedReader(new FileReader(filename));
            GraphMLReader2<Graph<MyVertex, MyEdge>, MyVertex, MyEdge>
                    graphReader = new GraphMLReader2<>
                                          (fileReader, graphTransformer, vertexTransformer,
                                                  edgeTransformer, hyperEdgeTransformer);

            Graph<MyVertex, MyEdge> g = graphReader.readGraph();
            Layout<MyVertex, MyEdge> layout = MyGraphUtils.getLayoutForLocations(g, vertexLocations);
            vertexLocations.clear();   //clearing map for next uses

            return layout;
         } catch (Exception e) {
            LOG.severe("Error during loading data from XML: " + e.getMessage());
         }
      }
      return null;
   }

   @SuppressWarnings("MagicConstant")
   private static int showFileDialog(int dialogType) {
      fileChooser.setDialogType(dialogType);
      return (dialogType == JFileChooser.SAVE_DIALOG) ? fileChooser.showSaveDialog(null) : fileChooser.showOpenDialog(null);
   }

   private static Transformer<GraphMetadata, Graph<MyVertex, MyEdge>> graphTransformer = new Transformer<GraphMetadata, Graph<MyVertex, MyEdge>>() {
      public Graph<MyVertex, MyEdge> transform(GraphMetadata metadata) {
         if (metadata.getEdgeDefault().equals(GraphMetadata.EdgeDefault.DIRECTED)) {
            return new DirectedSparseGraph<>();
         } else {
            LOG.severe("Error in XML: Only directed graphs are supported!");
            return null;
         }
      }
   };

   private static Transformer<NodeMetadata, MyVertex> vertexTransformer = new Transformer<NodeMetadata, MyVertex>() {
      public MyVertex transform(NodeMetadata metadata) {
         String name = metadata.getProperty(NAME);
         int time = Integer.parseInt(metadata.getProperty(DELAY));
         double x = Double.parseDouble(metadata.getProperty(X));
         double y = Double.parseDouble(metadata.getProperty(Y));

         MyVertex v = MyVertexFactory.getInstance().create(name, time);
         //set point locations to use in layout
         Point2D location = new Point2D.Double(x, y);
         vertexLocations.put(v, location);
         return v;
      }
   };

   private static Transformer<EdgeMetadata, MyEdge> edgeTransformer = new Transformer<EdgeMetadata, MyEdge>() {
      public MyEdge transform(EdgeMetadata metadata) {
         Integer registerCount = Integer.parseInt(metadata.getProperty(REGISTERS));
         return MyEdgeFactory.getInstance().create(registerCount);
      }
   };

   private static Transformer<HyperEdgeMetadata, MyEdge> hyperEdgeTransformer = new Transformer<HyperEdgeMetadata, MyEdge>() {
      public MyEdge transform(HyperEdgeMetadata metadata) {
         Integer registerCount = Integer.parseInt(metadata.getProperty(REGISTERS));
         return MyEdgeFactory.getInstance().create(registerCount);
      }
   };
}
