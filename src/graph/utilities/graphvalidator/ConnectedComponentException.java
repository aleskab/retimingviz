package graph.utilities.graphvalidator;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 9.04.14
 */
public class ConnectedComponentException extends GraphValidationException {
   public ConnectedComponentException(String message) {
      super(message);
   }
}
