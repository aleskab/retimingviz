package graph.utilities.graphvalidator;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import graph.elements.MyEdge;

import java.util.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 * <p/>
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 18.02.14
 */
public class GraphValidator<V, E> {

   private static GraphValidator instance;
   private Map<V, Mark> nodesAndMarksMap;
   private Graph<V, E> graph;
   private enum Mark {
      TEMP, PERMANENT, NOT_VISITED
   }

   private GraphValidator() { }

   public static void validate(final Graph graphToCheck) throws GraphValidationException {
      getInstance(graphToCheck).startValidation();
   }

   private static GraphValidator getInstance(Graph gr) {
      if (instance == null) instance = new GraphValidator<>();
      instance.graph = gr;
      return instance;
   }

   private void startValidation() throws GraphValidationException {
      checkIndependentGraphs();
      checkLoops();
      checkCycles();
      checkVertexSourcesAndSinks();
      checkParallelEdges();
   }

   /*
    * Checks of the graph contains loops. Throws exception if it does
    */
   private void checkLoops() throws LoopException {
      for (E e : graph.getEdges()) {
         Pair<V> p = graph.getEndpoints(e);
         if (p.getFirst().equals(p.getSecond())) {
            throw new LoopException("Graph contains loops!");
         }
      }
   }

   /*
    * For each vertex pair checks that there exist only 1 edge.
    * However graph with 2 vertices and 2 different edges is allowed.
    */
   private void checkParallelEdges() throws ParallelEdgeException {
      List<Pair<V>> vertexPairs = new ArrayList<>();
      for (E e : graph.getEdges()) {
         Pair<V> p = graph.getEndpoints(e);
         if (vertexPairs.contains(p)) {
            throw new ParallelEdgeException("Only 1 edge can exist between a vertex pair");
         } else {
            vertexPairs.add(p);
         }
      }
   }

   /*
    * Checks if graph contains sinks or sources. If contains, throws
    * exception according to what it has.
    * Sink - vertex with quantity of outgoing edges = 0.
    * Source - vertex with quantity of incoming edge = 0.
    */
   private void checkVertexSourcesAndSinks() throws SinkVertexException, SourceVertexException {
      for (V v : graph.getVertices()) {
         if (graph.getInEdges(v).isEmpty()) {
            throw new SourceVertexException("Graph contains Sources!");
         }
         if (graph.getOutEdges(v).isEmpty()) {
            throw new SinkVertexException("Graph contains Sinks!");
         }
      }
   }

   /**
    * Method utilises Tarjan Topological Sorting algorithm(1976)
    * Checks if graph (without registers!) is a DAG (Directed Acyclic Graph), if not - throws CyclicException
    * Based no pseudocode from: http://en.wikipedia.org/wiki/Topological_sorting
    */
   private void checkCycles() throws CyclicException {
      nodesAndMarksMap = new HashMap<>();
      for (V v : graph.getVertices()) {
         List<E> incidentEdges = getIncidentEdgesWithoutRegisters(v);
         if (!incidentEdges.isEmpty()) //if v is not standalone
            nodesAndMarksMap.put(v, Mark.NOT_VISITED);
      }
      while (!getUnmarkedNodesFromMap().isEmpty()) {
         V nodeToVisit = getUnmarkedNodesFromMap().iterator().next();
         visit(nodeToVisit);
      }
   }

   private void visit(V node) throws CyclicException {
      if (getMark(node).equals(Mark.TEMP)) {
         throw new CyclicException("Graph is not a DAG (directed acyclic graph)!");
      }
      if (getMark(node).equals(Mark.NOT_VISITED)) {
         mark(node, Mark.TEMP);
         for (E edge : getOutEdgesWithoutRegisters(node)) {
            visit(graph.getDest(edge));
         }
         mark(node, Mark.PERMANENT); //adding sorted node
      }
   }

   private List<V> getUnmarkedNodesFromMap() {
      List<V> l = new ArrayList<>();
      for (V obj : nodesAndMarksMap.keySet()) {
         if (getMark(obj).equals(Mark.NOT_VISITED)) {
            l.add(obj);
         }
      }
      return l;
   }

   private List<E> getIncidentEdgesWithoutRegisters(V vertex) {
      return getEdgesWithoutRegisters(graph.getIncidentEdges(vertex));
   }

   private List<E> getOutEdgesWithoutRegisters(V vertex) {
      return getEdgesWithoutRegisters(graph.getOutEdges(vertex));
   }

   private List<E> getEdgesWithoutRegisters(Collection<E> edges) {
      List<E> l = new ArrayList<>();
      for (E edge : edges) {
         if (!((MyEdge) edge).getHasRegister()) {
            l.add(edge);
         }
      }
      return l;
   }

   private Mark getMark(V vertex) {
      return nodesAndMarksMap.get(vertex);
   }

   private void mark(V node, Mark mark) {
      nodesAndMarksMap.put(node, mark);
   }

   /*
   *  Checks whether graph contains independent graphs inside.
   *  Exception is thrown if it is so.
   */
   private void checkIndependentGraphs() throws ConnectedComponentException {
      Map<V, Mark> marksMap = new HashMap<>();
      //get random vertice and start marking from that.
      V startNode = graph.getVertices().iterator().next();
      marksMap.put(startNode, Mark.PERMANENT);
      checkSuccessorsOfNode(graph.getSuccessors(startNode), marksMap);

      //if map.size < vertexCount --> has connected components > 1, throw exception
      if (marksMap.size() < graph.getVertexCount()) {
         List<V> nodes = new ArrayList<>(graph.getVertices());
         nodes.removeAll(marksMap.keySet());
         for (V node : nodes) {
            for (V successor : graph.getSuccessors(node)) {
               if (marksMap.containsKey(successor)) {
                  return;
               }
            }
         }
         throw new ConnectedComponentException("Graph has several connected components!");
      }
   }

   private void checkSuccessorsOfNode(final Collection<V> successors, Map<V, Mark> map) {
      List<V> unmarkedNodes = new ArrayList<>();
      for (V successor : successors) {
         if (map.get(successor) == null || map.get(successor).equals(Mark.NOT_VISITED)) {
            unmarkedNodes.add(successor);
         }
      }

      if (!unmarkedNodes.isEmpty()) {
         //mark unmarked and continue traversing
         for (V node : unmarkedNodes) {
            map.put(node, Mark.PERMANENT);
            checkSuccessorsOfNode(graph.getSuccessors(node), map);
         }
      }
   }
}
