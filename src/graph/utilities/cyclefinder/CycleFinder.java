package graph.utilities.cyclefinder;

import edu.uci.ics.jung.graph.Graph;

import java.util.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 13.03.14
 */
public class CycleFinder<V, E> {
   private final Graph<V, E> graph;
   private List<Set<V>> cycles;

   public CycleFinder(final Graph<V, E> g) {
      this.graph = g;
      this.cycles = new ArrayList<>();
   }

   public List<Set<V>> findCyclesAndPaths() {
      for (V v : graph.getVertices()) {
         checkPaths(v, v, new LinkedHashSet<V>());
      }
      return cycles;
   }

   private void checkPaths(V startVertex, V currentVertex, Set<V> path) {
      if (path.contains(currentVertex) && path.size() > 0) {
         if (currentVertex.equals(startVertex)) { //found cycle
            addCycle(path);
         } //else is just path, not a cycle
         return;
      }
      path.add(currentVertex); //push and then check all successors
      for (V successor : graph.getSuccessors(currentVertex)) {
         checkPaths(startVertex, successor, new LinkedHashSet<>(path));
      }
   }

   private void addCycle(Set<V> newCycle) {
      for (Set existingCycle : cycles) { //check if we already have cycle with same vertices
         if (newCycle.equals(existingCycle)) {
            return;
         }
      }
      this.cycles.add(newCycle);
   }
}
