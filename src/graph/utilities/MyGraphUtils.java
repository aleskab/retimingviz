package graph.utilities;

import common.EditingModalGraphMouseFixed;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import graph.StepDetails;
import graph.elements.MyEdge;
import graph.elements.MyEdgeFactory;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import org.apache.commons.collections15.Transformer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 13.02.14
 */
public abstract class MyGraphUtils {

   public static final Dimension defaultDimension = new Dimension(400, 400);

   public static DefaultModalGraphMouse getGraphMouseWithoutEdit() {
      DefaultModalGraphMouse dm = new DefaultModalGraphMouse<>();
      dm.setMode(ModalGraphMouse.Mode.PICKING);
      return dm;
   }

   public static VisualizationViewer<MyVertex, MyEdge> createGraphComponent(Graph<MyVertex, MyEdge> graph) {
      // The Layout<V, E> is parameterized by the vertex and edge types
      Layout<MyVertex, MyEdge> layout = new FRLayout<>(graph);
      layout.setSize(defaultDimension); // sets the initial size of the space

      // The BasicVisualizationServer<V,E> is parameterized by the edge types
      VisualizationViewer<MyVertex, MyEdge> vv = new VisualizationViewer<>(layout);
      vv.setPreferredSize(defaultDimension);
      // Show vertex and edge labels
      vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<MyVertex>());
      vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<MyEdge>());
      vv.getRenderContext().setEdgeFontTransformer(new Transformer<MyEdge, Font>() {
         @Override
         public Font transform(MyEdge myEdge) {
            return new Font("Verdana", Font.BOLD, 17);
         }
      });
      //mouse
      vv.setGraphMouse(getGraphMouseWithEditSupport(vv));
      return vv;
   }

   public static Layout<MyVertex, MyEdge> getLayoutForLocations(Graph<MyVertex, MyEdge> g, Map<MyVertex, Point2D> locations) {
      Layout<MyVertex, MyEdge> layout = new StaticLayout<>(g);
      for (MyVertex v : g.getVertices()) {
         layout.setLocation(v, locations.get(v));
      }
      return layout;
   }

   public static Map<MyVertex, Point2D> getVertexLocationsForLayout(Layout<MyVertex, MyEdge> l) {
      Map<MyVertex, Point2D> locations = new HashMap<>();
      for (Object v : l.getGraph().getVertices()) {
         locations.put((MyVertex) v, l.transform((MyVertex) v));
      }
      return locations;
   }

   public static Point2D getCenterPointForLayout(Layout<MyVertex, MyEdge> layout) {
      Map<MyVertex, Point2D> locations = getVertexLocationsForLayout(layout);
      List<Double> abs = new ArrayList<>();
      List<Double> ords = new ArrayList<>();
      for (Point2D point : locations.values()) {
         abs.add(point.getX());
         ords.add(point.getY());
      }
      Collections.sort(abs);
      Collections.sort(ords);

      Double centerX = 0.0;
      Double centerY = 0.0;
      if (!abs.isEmpty() && !ords.isEmpty()) {
         centerX = (abs.get(0) + abs.get(abs.size() - 1)) / 2;
         centerY = (ords.get(0) + ords.get(ords.size() - 1)) / 2;
      }
      return new Point2D.Double(centerX, centerY);
   }

   public static EditingModalGraphMouseFixed getGraphMouseWithEditSupport(VisualizationViewer<MyVertex, MyEdge> vv) {
      EditingModalGraphMouseFixed<MyVertex, MyEdge> egm = new EditingModalGraphMouseFixed<>(vv.getRenderContext(), MyVertexFactory.getInstance(), MyEdgeFactory.getInstance());
      egm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
      return egm;
   }

   public static Graph<MyVertex, MyEdge> createGraphFromExercise() {
      Graph<MyVertex, MyEdge> g = new DirectedSparseMultigraph<>();
      MyEdgeFactory edgeFactory = MyEdgeFactory.getInstance();
      MyVertexFactory vertexFactory = MyVertexFactory.getInstance();
      g.addVertex(vertexFactory.create("A", 10));
      g.addVertex(vertexFactory.create("B", 3));
      g.addVertex(vertexFactory.create("C", 4));
      g.addVertex(vertexFactory.create("D", 4));
      g.addVertex(vertexFactory.create("E", 2));
      g.addVertex(vertexFactory.create("F", 10));
      g.addVertex(vertexFactory.create("G", 3));

      ArrayList<MyVertex> vertexArrayList = new ArrayList<>(g.getVertices());
      Collections.sort(vertexArrayList, new Comparator<MyVertex>() {
         public int compare(MyVertex a, MyVertex b) {
            return a.getName().compareTo(b.getName());
         }
      });

      g.addEdge(edgeFactory.create(1), vertexArrayList.get(0), vertexArrayList.get(1));
      g.addEdge(edgeFactory.create(1), vertexArrayList.get(0), vertexArrayList.get(2));
      g.addEdge(edgeFactory.create(1), vertexArrayList.get(5), vertexArrayList.get(0));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(1), vertexArrayList.get(3));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(3), vertexArrayList.get(5));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(2), vertexArrayList.get(3));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(2), vertexArrayList.get(4));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(4), vertexArrayList.get(5));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(4), vertexArrayList.get(6));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(6), vertexArrayList.get(5));

      return g;
   }

   public static Graph<MyVertex, MyEdge> createSimpleTestGraph() {
      Graph<MyVertex, MyEdge> g = new DirectedSparseMultigraph<>();
      MyEdgeFactory edgeFactory = MyEdgeFactory.getInstance();
      MyVertexFactory vertexFactory = MyVertexFactory.getInstance();
      g.addVertex(vertexFactory.create("A", 7));
      g.addVertex(vertexFactory.create("B", 1));
      g.addVertex(vertexFactory.create("C", 2));
      g.addVertex(vertexFactory.create("D", 4));
      ArrayList<MyVertex> vertexArrayList = new ArrayList<>(g.getVertices());
      Collections.sort(vertexArrayList, new Comparator<MyVertex>() {
         public int compare(MyVertex a, MyVertex b) {
            return a.getName().compareTo(b.getName());
         }
      });
      g.addEdge(edgeFactory.create(1), vertexArrayList.get(3), vertexArrayList.get(0));
      g.addEdge(edgeFactory.create(1), vertexArrayList.get(0), vertexArrayList.get(2));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(0), vertexArrayList.get(1));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(1), vertexArrayList.get(3));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(2), vertexArrayList.get(1));
      g.addEdge(edgeFactory.create(0), vertexArrayList.get(2), vertexArrayList.get(3));

      return g;
   }

   public static void saveImageToPNGFile(Component owner, BufferedImage image) {
      JFileChooser c = new JFileChooser();
      c.setDialogType(JFileChooser.SAVE_DIALOG);
      c.setFileSelectionMode(JFileChooser.FILES_ONLY);

      int rVal = c.showSaveDialog(owner);
      if (rVal == JFileChooser.APPROVE_OPTION) {
         try {
            String filename = c.getSelectedFile().toString();
            if (!filename.endsWith(".png") || !filename.endsWith(".PNG")) {
               filename = filename.concat(".PNG");
            }
            ImageIO.write(image, "png", new File(filename));
         } catch (IOException ex) {
            Logger.getLogger(MyGraphUtils.class.getName()).severe("Could not export PNG image: " + ex.getMessage());
         }
      }
   }

   public static BufferedImage getImageFromComponent(Component component, VisualizationViewer vv) {
      Dimension size = component.getSize();
      Color background = vv.getBackground();
      vv.setBackground(Color.WHITE);
      BufferedImage myImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = myImage.createGraphics();
      component.paint(g2);
      vv.setBackground(background);
      return myImage;
   }

   public static Graph<MyVertex, MyEdge> getClone(final Graph<MyVertex, MyEdge> graphToClone) {
      Graph<MyVertex, MyEdge> g = new DirectedSparseMultigraph<>();
      for (MyVertex v : graphToClone.getVertices())
         g.addVertex(v);
      for (MyEdge e : graphToClone.getEdges())
         g.addEdge(e, graphToClone.getIncidentVertices(e));
      return g;
   }

   public static <V, E> boolean allowedToCreateEdge(final V startVertex, final V other, final Graph<V, E> g) {
      boolean edgeAlreadyExists = g.findEdge(startVertex, other) != null;
      return !edgeAlreadyExists && !other.equals(startVertex);
   }

   public static String toString(Graph<MyVertex, MyEdge> graph) {
      StringBuilder sb = new StringBuilder("\nVertices(quantity: ").append(graph.getVertexCount()).append(")\n     ");
      for (Object v : graph.getVertices()) {
         sb.append(v).append("   ");
      }
      sb.append("\nEND Vertices");
      sb.append("\nEdges (quantity: ").append(graph.getEdgeCount()).append(")\n");
      for (MyEdge e : graph.getEdges()) {
         Pair<MyVertex> ep = graph.getEndpoints(e);
         sb.append("   ").append(
                                        (e.toString().isEmpty()) ? "  " : e
         );
         sb.append("<").append(ep.getFirst()).append(",").append(ep.getSecond()).append(">\n");
      }
      sb.append("END Edges\n");
      return sb.toString();
   }

   public static VisualizationViewer<MyVertex, MyEdge> createComponentForCleared() {
      StepDetails details = StepDetails.getInstance();
      Layout<MyVertex, MyEdge> l = getLayoutForLocations(details.getClearedGraph(), details.getVertexLocations());
      l.setSize(defaultDimension);
      VisualizationViewer<MyVertex, MyEdge> vv = new VisualizationViewer<>(l);
      vv.setPreferredSize(defaultDimension);
      vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<MyVertex>());
      //hack: setting edge labeller, since cleared graph from StepDetails can contain registers
      vv.getRenderContext().setEdgeLabelTransformer(new Transformer<MyEdge, String>() {
         @Override
         public String transform(MyEdge myEdge) {
            return "";
         }
      });
      return vv;
   }

   public static int getRegistersCount(Graph<MyVertex, MyEdge> graph, Set<MyVertex> verticesOnPath) {
      int registers = 0;
      List<MyEdge> edges = getEdgesForPath(graph, verticesOnPath.toArray(new MyVertex[1]));
      for (MyEdge edge : edges) {
         registers += edge.getRegisterCount();
      }
      return registers;
   }

   public static List<MyEdge> getEdgesForPath(Graph<MyVertex, MyEdge> graph, MyVertex[] verticesOnPath) {
      List<MyVertex> path = new ArrayList<>(Arrays.asList(verticesOnPath));
      List<MyEdge> l = new ArrayList<>();
      for (MyEdge edge : graph.getEdges()) {
         //if edge sourceVertex is in vertices and destinationVertex is in vertices
         MyVertex srcVertex = graph.getSource(edge);
         MyVertex destVertex = graph.getDest(edge);
         int positionDiff = Math.abs(path.indexOf(destVertex) - path.indexOf(srcVertex));
         if (path.contains(srcVertex) && path.contains(destVertex) && (positionDiff == 1 || positionDiff == path.size() - 1)) {
            l.add(edge);
         }
      }
      return l;
   }
}
