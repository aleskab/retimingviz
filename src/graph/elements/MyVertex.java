package graph.elements;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.02.14
 */
public class MyVertex implements Comparable {
   private String name;
   private int propagationDelay;

   public MyVertex(String name, int propagationDelay) {
      this.name = name;
      this.propagationDelay = propagationDelay;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      if (org.apache.commons.lang.StringUtils.isNotEmpty(name) && !MyVertexFactory.hasName(name)) {
         this.name = name;
      } else {
         if (null != name)
            javax.swing.JOptionPane.showMessageDialog(null, "This name is not available ..."
                                              , "Inappropriate name!", javax.swing.JOptionPane.WARNING_MESSAGE);
      }
   }

   @Override
   public String toString() {
      return name.toUpperCase() + "(t=" + propagationDelay + ")";
   }

   public int getPropagationDelay() {
      return propagationDelay;
   }

   public void setPropagationDelay(int propagationDelay) {
      this.propagationDelay = propagationDelay;
   }

   @Override
   public int compareTo(Object o) {
      return this.toString().compareTo(o.toString());
   }
}

