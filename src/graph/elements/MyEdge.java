package graph.elements;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.02.14
 */
public class MyEdge {

   public boolean getHasRegister() {
      return registerCount > 0;
   }

   public int getRegisterCount() {
      return registerCount;
   }

   private int registerCount;

   public MyEdge(int registers) {
      this.registerCount = registers;
   }

   @Override
   public String toString() {
      return (getHasRegister()) ? "RG:" + registerCount : "";
   }

   public void addOneRegister() {
      this.registerCount++;
   }

   public void removeOneRegister() {
      this.registerCount--;
   }

   public void removeAllRegisters() {
      this.registerCount = 0;
   }
}
