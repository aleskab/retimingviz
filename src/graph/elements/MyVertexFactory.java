package graph.elements;

import org.apache.commons.collections15.Factory;
import org.apache.commons.lang.StringUtils;
import ui.UiUtilities;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.02.14
 */
public class MyVertexFactory implements Factory<MyVertex> {
   private static MyVertexFactory instance = new MyVertexFactory();
   private static Set<String> names = new HashSet<>();

   public static boolean hasName(String name) {
      return names.contains(name.toUpperCase());
   }

   public static boolean removeName(String name) {
      return names.remove(name.toUpperCase());
   }

   public void clearAllowedNames() {
	   names.clear();
   }

   private void addName(String name) {
      names.add(name.toUpperCase());
   }

   private MyVertexFactory() { }

   public static MyVertexFactory getInstance() {
      return instance;
   }

   public MyVertex create() {
      String name = "";
      while (name != null && StringUtils.isBlank(name)) {
         name = JOptionPane.showInputDialog("Enter Name for new Vertex");
         while (StringUtils.isNotBlank(name) && hasName(name)) {
            name = JOptionPane.showInputDialog("There is such name, pick another!");
         }
      }
      if (name == null) return null; //if cancelled, do not create

      Integer time = UiUtilities.getIntValueFromUser(null, "Enter Time for Data:");
      if (time == null) return null; //if cancelled do not create

      return create(name, time);
   }

   public MyVertex create(String name, int dataReadyTime) {
      addName(name);
      return new MyVertex(name, dataReadyTime);
   }
}

