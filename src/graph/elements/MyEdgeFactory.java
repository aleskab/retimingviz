package graph.elements;

import org.apache.commons.collections15.Factory;
import ui.UiUtilities;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 14.02.14
 */
public class MyEdgeFactory implements Factory<MyEdge> {

   private static MyEdgeFactory instance = new MyEdgeFactory();

   private MyEdgeFactory() {
   }

   public static MyEdgeFactory getInstance() {
      return instance;
   }

   public MyEdge create() {
      Integer edges = UiUtilities.getIntValueFromUser(null, "How many registers?");
      if (edges == null) return null;
      return create(edges);
   }

   public MyEdge create(int registers) {
      return new MyEdge(registers);
   }

}

