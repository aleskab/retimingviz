package graph;


import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractModalGraphMouse;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.MyGraphUtils;
import graph.utilities.graphvalidator.GraphValidationException;
import graph.utilities.graphvalidator.GraphValidator;
import graph.utilities.graphvalidator.RetimingAlgorithmException;
import ui.parts.ClockTimePanel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.logging.Logger;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 25.10.13
 */
public class MyGraph {
   private static final Logger LOG = Logger.getLogger(MyGraph.class.getName());
   private AbstractModalGraphMouse editingModalGraphMouse;
   private Graph<MyVertex, MyEdge> originalGraph;
   private static int TIME_REQUIREMENT_FOR_RETIMING = 15;
   private static int stepNum = 0;
   private VisualizationViewer<MyVertex, MyEdge> vv;

   public static void setTimeRequirement(int time) {
      TIME_REQUIREMENT_FOR_RETIMING = time;
      stepNum = 0;
   }

   public AbstractModalGraphMouse getEditingModalGraphMouse() {
      return editingModalGraphMouse;
   }

   public Graph<MyVertex, MyEdge> getOriginalGraph() {
      return originalGraph;
   }

   public void setOriginalGraph(Graph<MyVertex, MyEdge> g) {
      this.originalGraph = g;
   }

   public MyGraph() {
      originalGraph = MyGraphUtils.createGraphFromExercise();
      LOG.info("created Graph = " + MyGraphUtils.toString(originalGraph));
      this.vv = MyGraphUtils.createGraphComponent(originalGraph);

      //set Mouse support for visualization component
      // Create a graph mouse and add it to the visualization viewer
      this.editingModalGraphMouse = MyGraphUtils.getGraphMouseWithEditSupport(this.vv);
      this.vv.setGraphMouse(editingModalGraphMouse);
      // Add the mouses mode key listener to work it needs to be added to the visualization component
      this.vv.addKeyListener(editingModalGraphMouse.getModeKeyListener());
      vv.addMouseMotionListener(new HighlightAdapter());
   }

   private class HighlightAdapter extends MouseMotionAdapter {
      @Override
      public void mouseMoved(MouseEvent e) {
         if (ClockTimePanel.isNotActive()) { //if ClockTimePanel is active, just show selected cycle if any.
            Object edgeAtPoint = vv.getPickSupport().getEdge(vv.getGraphLayout(), (double) e.getX(), (double) e.getY());
            if (edgeAtPoint != null) {
               vv.getRenderContext().getPickedEdgeState().pick((MyEdge) edgeAtPoint, true);
            } else {
               vv.getRenderContext().getPickedEdgeState().clear();
            }

            Object vertexAtPoint = vv.getPickSupport().getVertex(vv.getGraphLayout(), (double) e.getX(), (double) e.getY());
            if (vv.getPickedVertexState().getPicked().size() < 2) {
               if (vertexAtPoint != null) {
                  vv.getRenderContext().getPickedVertexState().pick((MyVertex) vertexAtPoint, true);
               } else {
                  vv.getRenderContext().getPickedVertexState().clear();
               }
            }
         }
      }
   }

   public VisualizationViewer<MyVertex, MyEdge> getVisualizationViewer() {
      return vv;
   }

   public boolean startRetiming() throws GraphValidationException, RetimingAlgorithmException {
      GraphValidator.validate(originalGraph); //if not valid, exception is thrown

      if (stepNum > originalGraph.getVertexCount()) { //if step > number of node, no feasible retiming exist (sic!)
         LOG.severe(MyGraphUtils.toString(getOriginalGraph()) + "\nNO FEASIBLE RETIMING EXIST!");
         stepNum = 0; //reset counter
         throw new RetimingAlgorithmException("No feasible retiming exist. Number of step > Number of nodes ...");
      }
      LOG.info("Validation Passed!");
      LOG.info(MyGraphUtils.toString(getOriginalGraph()) + "\nStarting Retiming iteration!");

      boolean isDone = false;
      Graph<MyVertex, MyEdge> graphToRetime = MyGraphUtils.getClone(originalGraph);

      //(1) clear all registers(edgeswhere hasRegister = true)
      Graph<MyVertex, MyEdge> graphWithClearedRegisterEdges = clearEdgesWithRegisters(graphToRetime);
      //(2) find data readiness in nodes, considering that combinatorial inputs have data, graphWithoutRegisters!
      Map<MyVertex, Integer> map = findDataReadinessForNodes(graphWithClearedRegisterEdges);
      //(3) mark all nodes, where time criteria is not met, if it is met for all nodes, then END
      ArrayList<MyVertex> nodesToRetime = getNodesToRetime(map);
      setStepDetails(graphWithClearedRegisterEdges, map, nodesToRetime); //set details to be used in IterationInfoDialog
      if (nodesToRetime.size() == 0) {
         isDone = true;
      }
      //(4) restore registers and fulfill retiming for those nodes one by one. (Shift register from node output to its input)
      //change the graph
      if (!isDone) {
         graphToRetime = retimeNodesInGraph(nodesToRetime, graphToRetime);
      }
      printMapDataToConsole(map, MyGraphUtils.toString(graphToRetime), nodesToRetime.toString(), isDone);
      stepNum = isDone ? 0 : stepNum + 1;
      return isDone;
   }

   private void setStepDetails(Graph<MyVertex, MyEdge> cleared, Map<MyVertex, Integer> dataReadinessMap, List<MyVertex> nodesToRetime) {
      Map<MyVertex, Point2D> vertexLocationsForStep = MyGraphUtils.getVertexLocationsForLayout(this.vv.getGraphLayout());
      StepDetails.getInstance().setDetails(cleared, dataReadinessMap, nodesToRetime, vertexLocationsForStep, TIME_REQUIREMENT_FOR_RETIMING);
   }

   private void printMapDataToConsole(Map<MyVertex, Integer> map, String graphString, String nodesString, boolean isDone) {
      StringBuilder sb = new StringBuilder();
      if (isDone) sb.append("### FINAL ");
      sb.append("STEP ").append(stepNum).append(" ##################################################\n");
      for (Map.Entry<MyVertex, Integer> entry : map.entrySet()) {
         if (isDone) sb.append("### DONE! -> ");
         sb.append("node: ").append(entry.getKey().getName().toUpperCase()).append(" time: ").append(entry.getValue()).append("\n");
      }
      sb.append("Time_Requirement is ").append(TIME_REQUIREMENT_FOR_RETIMING).append("\n");
      sb.append("_________________________________\n");
      sb.append(nodesString).append("\n");
      sb.append("_________________________________\n");
      sb.append(graphString).append("\n");
      sb.append("__________________________End Of Step ").append(stepNum).append("\n");
      if (isDone) {
         sb.append("#####################  DONE RETIMING!  ##################\n");
         sb.append("########################################################\n");
      }
      LOG.info(sb.toString());
   }

   private Graph<MyVertex, MyEdge> retimeNodesInGraph(List<MyVertex> nodesToRetime, final Graph<MyVertex, MyEdge> graph) {
      //put all from out to in
      for (MyVertex node : nodesToRetime) {
         for (MyVertex successor : graph.getSuccessors(node)) {
            if (nodesToRetime.contains(successor)) {
               int successorIndex = nodesToRetime.indexOf(successor);
               if (nodesToRetime.indexOf(node) < successorIndex) {
                  nodesToRetime.set(nodesToRetime.indexOf(node), successor);
                  nodesToRetime.set(successorIndex, node);
               }
            }
         }
      }

      for (MyVertex node : nodesToRetime) {
         try {
            //Removing register from outputs
            Iterator<MyEdge> i = graph.getOutEdges(node).iterator();
            while (i.hasNext()) {
               i.next().removeOneRegister(); //remove register from output;
            }
            //put to inputs
            i = graph.getInEdges(node).iterator();
            while (i.hasNext()) {
               i.next().addOneRegister();
            }
         } catch (Exception e) {
            LOG.severe("Couldn`t retime output of node: " + node.toString());
         }
      }
      return graph;
   }

   private ArrayList<MyVertex> getNodesToRetime(Map<MyVertex, Integer> map) {
      ArrayList<MyVertex> nodes = new ArrayList<>();
      for (MyVertex vertex : map.keySet()) {
         if (map.get(vertex) > TIME_REQUIREMENT_FOR_RETIMING) {
            nodes.add(vertex);
         }
      }
      return nodes;
   }

   private Map<MyVertex, Integer> findDataReadinessForNodes(final Graph<MyVertex, MyEdge> clearedGraph) {
      Map<MyVertex, Integer> dataReadynessMap = new TreeMap<>();

      //calculate for start points
      Set<MyVertex> nodes = new HashSet<>(clearedGraph.getVertices());
      for (MyEdge e : clearedGraph.getEdges()) {
         if (clearedGraph.getDest(e) != null)
            nodes.remove(clearedGraph.getDest(e));
      }
      for (MyVertex startVertex : nodes) {
         dataReadynessMap.put(startVertex, startVertex.getPropagationDelay());
      }
      //[END]startpoints [ END ]
      //Calculate for successors while they exist
      while (!nodes.isEmpty()) {
         nodes = calculateTimesForSuccessors(nodes, clearedGraph, dataReadynessMap);
      }
      return dataReadynessMap;
   }

   private Set<MyVertex> calculateTimesForSuccessors(final Set<MyVertex> nodes, final Graph<MyVertex, MyEdge> graph, Map<MyVertex, Integer> dataMap) {
      //Get the successors of input nodes from graph
      Set<MyVertex> successors = new HashSet<>();
      for (MyVertex currentNode : nodes) {
         for (MyVertex successor : graph.getSuccessors(currentNode)) {
            successors.add(successor);
            int timeForSuccessor = getTimeForVertex(currentNode, dataMap) + (successor).getPropagationDelay();
            if (dataMap.get(successor) != null) {
               timeForSuccessor = Math.max(timeForSuccessor, dataMap.get(successor));
            }
            dataMap.put(successor, timeForSuccessor);
         }
      }
      return successors;
   }

   private int getTimeForVertex(MyVertex vertex, Map<MyVertex, Integer> map) {
      int time = vertex.getPropagationDelay();
      if (map.get(vertex) != null) {
         time = Math.max(time, map.get(vertex));
      }
      return time;
   }

   private Graph<MyVertex, MyEdge> clearEdgesWithRegisters(final Graph<MyVertex, MyEdge> graph) {
      Graph<MyVertex, MyEdge> clone = MyGraphUtils.getClone(graph);
      LOG.info("Clearing Edges with registers");
      for (MyEdge currentEdge : getOriginalGraph().getEdges()) {
         if (currentEdge.getHasRegister()) {
            clone.removeEdge(currentEdge);
         }
      }
      return clone;
   }
}
