package ui;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import graph.StepDetails;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.FileManager;
import graph.utilities.MyGraphUtils;
import graph.utilities.graphgenerator.Generator;
import ui.parts.ClockTimePanel;
import ui.parts.DataInfoPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static ui.UiUtilities.getIntValueFromUser;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class DetailsPanel extends JPanel {
   MainSplitPane owner;
   private GridBagConstraints layoutContraints = new GridBagConstraints() {{
      fill = GridBagConstraints.HORIZONTAL;
      weighty = 0.1;
      ipady = 50;
      ipadx = 10;
   }};

   public DetailsPanel(final MainSplitPane mainSplitPane) {
      this.owner = mainSplitPane;
      setLayout(new GridBagLayout());
      addShowInfoButton();
      addSaveLoadStateButtons();
      addSaveImageButton();
      addShowTheoreticalTimeButton();
      addRandomGraphButton();
   }

   private GridBagConstraints getLayoutConstraints(int x, int y, int width) {
      layoutContraints.gridx = x;
      layoutContraints.gridy = y;
      layoutContraints.gridwidth = width;
      return layoutContraints;
   }

   private void addShowTheoreticalTimeButton() {
      JButton showTheoreticalTime = new JButton("Show theoretical time for cycles", UIManager.getIcon("FileChooser.listViewIcon"));
      showTheoreticalTime.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            new ClockTimePanel(owner);
         }
      });
      this.add(showTheoreticalTime, getLayoutConstraints(0, 4, 2));
   }

   private void addSaveLoadStateButtons() {
      JButton saveButton = new JButton("Save Graph", UIManager.getIcon("FileChooser.floppyDriveIcon"));
      saveButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent event) {
            Layout<MyVertex, MyEdge> data = owner.getInitPanel().getSaveDataForGraph();
            FileManager.saveState(data);
         }
      });
      this.add(saveButton, getLayoutConstraints(0, 0, 1));

      JButton loadButton = new JButton("Load Graph", UIManager.getIcon("FileChooser.directoryIcon"));
      loadButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent event) {
            Layout<MyVertex, MyEdge> data = FileManager.loadState();
            if (data != null) {
               owner.getInitPanel().setGraphFromXML(data);
            }
         }
      });
      this.add(loadButton, getLayoutConstraints(1, 0, 1));
   }

   private void addRandomGraphButton() {
      JButton random = new JButton("Generate graph");
      random.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent event) {
            final Integer verticesNum = getIntValueFromUser(owner.getDetailsPanel(), "Enter number of Vertices for new graph between [3 and 15]:");
            if (verticesNum != null) {
               if (verticesNum < 3) {
                  JOptionPane.showMessageDialog(owner, "Value should be greater than 2", "Value too Small", JOptionPane.WARNING_MESSAGE);
                  return;
               } else if (verticesNum > 15) {
                  int option = JOptionPane.showConfirmDialog(owner, "Number of vertices is greater than 15. Generation can be extremely(!!!) slow." +
                                                                            "\nAre you prepared for a coffee break?", "Do you want to continue?"
                                                                    , JOptionPane.YES_NO_OPTION);
                  if (option != 0) return;
               }
               Graph<MyVertex, MyEdge> g = Generator.generate(verticesNum, owner);
               if (g != null) {
                  owner.getInitPanel().setGraphFromGenerator(g);
               }
            }
         }
      });
      this.add(random, getLayoutConstraints(0, 1, 2));
   }

   private void addSaveImageButton() {
      JButton saveImageButton = new JButton("Export graph image", UIManager.getIcon("FileChooser.fileIcon"));
      saveImageButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            MyGraphUtils.saveImageToPNGFile(owner, owner.getInitPanel().getMyGraphImage());
         }
      });
      this.add(saveImageButton, getLayoutConstraints(0, 2, 2));
   }

   private void addShowInfoButton() {
      JButton showInfoButton = new JButton("Show information for iteration", UIManager.getIcon("FileChooser.detailsViewIcon"));
      showInfoButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (StepDetails.getInstance().getClearedGraph() != null && StepDetails.getInstance().getDataReadinessMap() != null)
               dataInfoPanel = new DataInfoPanel(owner.getOwnerFrame());
            else
               JOptionPane.showMessageDialog(owner.getOwnerFrame(), "Make retiming first!", "NO DATA IS AVAILABLE ...", JOptionPane.WARNING_MESSAGE);
         }
      });
      this.add(showInfoButton, getLayoutConstraints(0, 3, 2));
   }

   private DataInfoPanel dataInfoPanel;

   public void updateData() {
      dataInfoPanel.updateDataInPanel();
   }

   public boolean isInfoDialogVisible() {
      return (dataInfoPanel != null) && dataInfoPanel.isVisible();
   }
}
