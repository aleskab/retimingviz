package ui;

import common.EditingModalGraphMouseFixed;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.DAGLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.picking.PickedState;
import graph.MyGraph;
import graph.StepDetails;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.elements.MyVertexFactory;
import graph.utilities.MyGraphUtils;
import graph.utilities.graphvalidator.GraphValidationException;
import graph.utilities.graphvalidator.RetimingAlgorithmException;
import ui.parts.TimeSelector;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class InitPanel extends JPanel {

   private MyGraph myGraph;
   private TimeSelector timeSelector;

   public InitPanel() {
      setLayout(new BorderLayout());

      this.myGraph = new MyGraph();

      add(myGraph.getVisualizationViewer(), BorderLayout.CENTER);

      timeSelector = new TimeSelector();
      add(timeSelector, BorderLayout.BEFORE_FIRST_LINE);
   }

   public BufferedImage getMyGraphImage() {
      BufferedImage img =  MyGraphUtils.getImageFromComponent(this, getVisViewer());
      //wysiwyg image from initPanel with cropped TimeSlider elements
      int newImgHeight = img.getHeight() - timeSelector.getLabelHeight() - timeSelector.getHeight();
      img = img.getSubimage(0, timeSelector.getHeight(), img.getWidth(), newImgHeight);
      return img;
   }

   public JMenu getMyGraphMouseModeMenu() {
      return myGraph.getEditingModalGraphMouse().getModeMenu();
   }

   public JComboBox getMyGraphMouseComboBox() {
      return myGraph.getEditingModalGraphMouse().getModeComboBox();
   }

   public void retimeMyGraph(boolean isRetimeOneStep) {
      boolean retimed = false;
      try {
         myGraph.setOriginalGraph(getGraphFromPanel());
         if (isRetimeOneStep) {
            if (myGraph.startRetiming()) { //retime one step
               retimed = true;
            }
         } else {
            while (!retimed) { //retime till done
               retimed = myGraph.startRetiming();
            }
         }
      } catch (GraphValidationException e) {
         JOptionPane.showMessageDialog(this, "Validation Failed: " + e.getMessage(), "Can not retime ...", JOptionPane.WARNING_MESSAGE);
      } catch (RetimingAlgorithmException rae) {
         JOptionPane.showMessageDialog(this, rae.getMessage(), "Algorithm error!", JOptionPane.ERROR_MESSAGE);
      } finally {
         repaint();
         if (retimed) {
            JOptionPane.showMessageDialog(this, "Finished", "Retiming is done.", JOptionPane.INFORMATION_MESSAGE);
         }
      }
   }

   public void centerGraphLocation() {
      VisualizationViewer<MyVertex, MyEdge> vv = getVisViewer();
      VisualizationViewer.GraphMouse gm = vv.getGraphMouse();
      if (gm instanceof EditingModalGraphMouseFixed) {
         ((EditingModalGraphMouseFixed) gm).centerGraphLocation(vv);
      }
   }

   public Layout<MyVertex, MyEdge> getSaveDataForGraph() {
      return myGraph.getVisualizationViewer().getGraphLayout();
   }

   public void setGraphFromXML(Layout<MyVertex, MyEdge> l) {
      MyVertexFactory.getInstance().clearAllowedNames();
      myGraph.getVisualizationViewer().setGraphLayout(l);
      myGraph.setOriginalGraph(l.getGraph());
      repaint();
   }

   public Graph<MyVertex, MyEdge> getGraphFromPanel() {
      return getVisViewer().getGraphLayout().getGraph();
   }

   protected void setGraphFromGenerator(Graph<MyVertex, MyEdge> g) {
      MyVertexFactory.getInstance().clearAllowedNames();
      StepDetails.getInstance().clearData();
      myGraph.getVisualizationViewer().getGraphLayout().setGraph(g);
 
      myGraph.getVisualizationViewer().setGraphLayout(new CircleLayout<>(g));  
      myGraph.getVisualizationViewer().getGraphLayout().initialize();
      
      myGraph.setOriginalGraph(g);
      repaint();
   }

   @SuppressWarnings("unchecked")
   private VisualizationViewer<MyVertex, MyEdge> getVisViewer() {
      return (VisualizationViewer<MyVertex, MyEdge>) getComponent(0);
   }

   public void highlightPath(Set<MyVertex> vertices) {
      Graph<MyVertex, MyEdge> g = getGraphFromPanel();
      PickedState<MyEdge> pickedEdges = getVisViewer().getPickedEdgeState();
      PickedState<MyVertex> pickedVertices = getVisViewer().getPickedVertexState();
      pickedEdges.clear();
      pickedVertices.clear();

      for (MyVertex v : vertices) {
         pickedVertices.pick(v, true);
      }

      List<MyEdge> edges = MyGraphUtils.getEdgesForPath(g, vertices.toArray(new MyVertex[1]));
      for (MyEdge edge : edges) {
         pickedEdges.pick(edge, true);
      }
   }
}
