package ui;

import javax.swing.*;
import java.awt.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 12.03.14
 */
public abstract class UiUtilities {

   public static Integer getIntValueFromUser(Component component, String inputMessage) {
      while (true) {
         try {
            String input = JOptionPane.showInputDialog(component, inputMessage);
            if (input == null) {
               return null;
            }
            return Integer.parseInt(input);
         } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(component, "Please enter a valid integer value ... ", "Wrong format!", JOptionPane.WARNING_MESSAGE);
         }
      }
   }
}
