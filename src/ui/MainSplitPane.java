package ui;

import javax.swing.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class MainSplitPane extends JSplitPane {
   private DetailsPanel detailsPanel = new DetailsPanel(this);

   private InitPanel initPanel = new InitPanel();

   public JFrame getOwnerFrame() {
      return ownerFrame;
   }

   private JFrame ownerFrame;

   public void toggleVisibilityOfDetails() {
      getRightComponent().setVisible(!getRightComponent().isVisible());
      resetToPreferredSizes();
   }

   public boolean isDetailsPanelVisible() {
      return getRightComponent().isVisible();
   }

   public DetailsPanel getDetailsPanel() {
      return detailsPanel;
   }

   public InitPanel getInitPanel() {
      return initPanel;
   }

   public MainSplitPane(JFrame frame) {
      this.ownerFrame = frame;
      setLeftComponent(initPanel);
      setRightComponent(detailsPanel);
      setDividerLocation(JSplitPane.HORIZONTAL_SPLIT);
      setOneTouchExpandable(true);
      setResizeWeight(1.0);
      getRightComponent().setVisible(false);
   }
}
