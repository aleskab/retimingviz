package ui.parts;

import edu.uci.ics.jung.graph.Graph;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.MyGraphUtils;
import graph.utilities.cyclefinder.CycleFinder;
import ui.InitPanel;
import ui.MainSplitPane;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Set;

import static java.awt.event.KeyEvent.VK_ESCAPE;
import static javax.swing.KeyStroke.getKeyStroke;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class ClockTimePanel extends JDialog {
   private InitPanel initPanel;
   private static boolean isOpened = false;

   public static boolean isNotActive() {
      return !isOpened;
   }

   public ClockTimePanel(MainSplitPane owner) {
      super(owner.getOwnerFrame(), "Clock Times", false);
      this.initPanel = owner.getInitPanel();
      JPanel panel = createPanelWithData();
      getContentPane().add(panel);
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      this.addWindowListener(new WindowAdapter() {
         @Override
         public void windowClosed(WindowEvent e) {
            isOpened = false;
         }

         @Override
         public void windowOpened(WindowEvent e) {
            isOpened = true;
         }
      });
      setResizable(true);
      setModalityType(ModalityType.MODELESS);
      pack();
      setLocationRelativeTo(owner);
      setVisible(true);
   }

   private JPanel createPanelWithData() {
      JPanel myPanel = new JPanel();
      myPanel.setLayout(new BorderLayout());
      myPanel.registerKeyboardAction(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            dispose();
         }
      }, getKeyStroke(VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
      updateDataInPanel(myPanel);
      return myPanel;
   }

   public void updateDataInPanel(JPanel panel) {
      Graph<MyVertex, MyEdge> g = initPanel.getGraphFromPanel();
      if (g != null) {
         List<Set<MyVertex>> cycles = new CycleFinder<>(g).findCyclesAndPaths();
         JTable table = new JTable(new InfoTableModel(cycles)) {
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
               Component c = super.prepareRenderer(renderer, row, column);
               if (isRowSelected(row)) {
                  c.setFont(getFont().deriveFont(Font.BOLD));
                  @SuppressWarnings("unchecked")
                  Set<MyVertex> vertices = (Set<MyVertex>) getValueAt(row, 0);
                  initPanel.highlightPath(vertices);
               }
               return c;
            }
         };
         table.getTableHeader().setReorderingAllowed(false);
         table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
         table.getColumnModel().getColumn(0).setPreferredWidth(300);
         table.setFillsViewportHeight(true);
         JScrollPane scrollPane = new JScrollPane(table);
         panel.add(scrollPane, BorderLayout.CENTER);
      }
   }

   private class InfoTableModel extends AbstractTableModel {
      private List<Set<MyVertex>> cyclesList;
      private String[] colNames = new String[]{"Cycle", "Delay", "Registers", "Synchronous Delay"};

      public InfoTableModel(List<Set<MyVertex>> cycles) {
         this.cyclesList = cycles;
      }

      @Override
      public String getColumnName(int col) {
         return colNames[col];
      }

      @Override
      public int getRowCount() {
         return cyclesList.size();
      }

      @Override
      public int getColumnCount() {
         return colNames.length;
      }

      @Override
      public Object getValueAt(int rowIndex, int columnIndex) {
         Object value = null;
         Set<MyVertex> cycle = cyclesList.get(rowIndex);
         int cycleDelay = findDelay(cycle);
         switch (columnIndex) {
            case 0:
               value = cycle;
               break;
            case 1:
               value = cycleDelay;
               break;
            case 2:
               value = MyGraphUtils.getRegistersCount(initPanel.getGraphFromPanel(), cycle);
               break;
            case 3:
               value = findTheoreticalTime(cycle, cycleDelay);
               break;
         }
         return value;
      }

      private int findDelay(Set<MyVertex> cycle) {
         int delay = 0;
         for (MyVertex v : cycle) {
            delay += v.getPropagationDelay();
         }
         return delay;
      }

      /*
       * Theoretical = cycleDelay / number of registers in cycle
       * Round to floor. (13/2 = 7)
       */
      private int findTheoreticalTime(Set<MyVertex> path, Integer pathDelay) {
         int registersNum = MyGraphUtils.getRegistersCount(initPanel.getGraphFromPanel(), path);
         return (registersNum != 0) ? pathDelay / registersNum : pathDelay;
      }
   }
}
