package ui.parts;

import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import graph.StepDetails;
import graph.elements.MyEdge;
import graph.elements.MyVertex;
import graph.utilities.MyGraphUtils;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import static java.awt.event.KeyEvent.VK_ESCAPE;
import static javax.swing.KeyStroke.getKeyStroke;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class DataInfoPanel extends JDialog {
   private static Dimension PANEL_SIZE = null;
   private JTable table;
   private JPanel panel;
   private JMenuItem saveImgMenuItem;
   private VisualizationViewer<MyVertex, MyEdge> vv;

   public DataInfoPanel(Frame owner) {
      super(owner, "Iteration Details", false);
      createPanelWithData();
      getContentPane().addComponentListener(new ComponentAdapter() {
        	  @Override
        	public void componentResized(ComponentEvent e) {
        		// TODO Auto-generated method stub
        		PANEL_SIZE = getSize();
        	}
      }); 
      getContentPane().add(new JScrollPane(panel));
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      setResizable(true);
      pack();
      setLocationRelativeTo(owner);      
      setVisible(true);
   }

   private JPanel createPanelWithData() {
      JPanel myPanel = new JPanel();
      myPanel.registerKeyboardAction(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            dispose();
         }
      }, getKeyStroke(VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

      myPanel.setLayout(new BorderLayout());

      this.panel = myPanel;
      createMenuItem();
      updateDataInPanel();
      return myPanel;
   }

   private void createMenuItem() {
      this.saveImgMenuItem = new JMenuItem("Save Data To PNG");
      this.saveImgMenuItem.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            BufferedImage img = MyGraphUtils.getImageFromComponent(panel, vv);
            MyGraphUtils.saveImageToPNGFile(panel, img);
         }
      });
   }

   private void updateGraph() {
      vv = MyGraphUtils.createComponentForCleared();
      //set mouse mode
      DefaultModalGraphMouse dm = MyGraphUtils.getGraphMouseWithoutEdit();
      dm.getModeMenu().add(this.saveImgMenuItem);
      vv.setGraphMouse(dm);
      vv.setComponentPopupMenu(dm.getModeMenu().getPopupMenu());
      vv.getPickedVertexState().addItemListener(new ItemListener() {
         @Override
         public void itemStateChanged(ItemEvent e) {
            table.clearSelection();
            if (e.getStateChange() == ItemEvent.SELECTED) {// SELECTED vertex
               int rowToSelect = ((InfoTableModel) table.getModel()).getIndexOfVertex(e.getItem());
               table.addRowSelectionInterval(rowToSelect, rowToSelect);
            }
         }
      });
      this.panel.add(vv, BorderLayout.PAGE_START);  
   }

   private void updateTable() {
      TableModel model = new InfoTableModel();
      this.table = new JTable(model) {
         @Override
         public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            Component c = super.prepareRenderer(renderer, row, column);
            int time = Integer.parseInt(getValueAt(row,2).toString());
            boolean isRequirementNotMet = time > StepDetails.getInstance().getTimeReq();
            
            if (isRowSelected(row)) {
               c.setFont(getFont().deriveFont(Font.BOLD));
               Object vertexObj = getValueAt(row, 1);
               if (vertexObj instanceof MyVertex) {
                  vv.getPickedVertexState().clear();
                  vv.getPickedVertexState().pick((MyVertex) vertexObj, true);
                  
                 
               }
            } else {
            	 if (isRequirementNotMet) {
                	c.setBackground(Color.RED.brighter());
                	c.setFont(getFont().deriveFont(Font.BOLD));
                 } else {
                	 c.setBackground(Color.WHITE);
                 }
            }
            return c;
         }

         {
            getColumnModel().getColumn(0).setMaxWidth(0);
            setTableHeader(null);
            setSelectionBackground(Color.LIGHT_GRAY);
            setSelectionForeground(Color.RED);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
         }
      };
      panel.add(table);
   }

   public void updateDataInPanel() {
      panel.removeAll();
      updateGraph();
      updateTable();
      pack();
      
      if (PANEL_SIZE != null) {
    	  setSize(PANEL_SIZE);
      }
      panel.repaint();
      repaint(); 
   }

   private class InfoTableModel extends AbstractTableModel {
      final StepDetails details = StepDetails.getInstance();
      final java.util.List mapKeys = Arrays.asList(details.getDataReadinessMap().keySet().toArray());

      public int getIndexOfVertex(Object obj) {
         return mapKeys.indexOf(obj);
      }

      @Override
      public int getRowCount() {
         return details.getDataReadinessMap().size();
      }

      @Override
      public int getColumnCount() {
         return 3;
      }

      @Override
      public Object getValueAt(int rowIndex, int columnIndex) {
         Object value = null;
         switch (columnIndex) {
            case 1:
               value = mapKeys.get(rowIndex);
               break;
            case 2:
               value = details.getDataReadinessMap().get(mapKeys.get(rowIndex));
               break;
         }
         return value;
      }
   }
}
