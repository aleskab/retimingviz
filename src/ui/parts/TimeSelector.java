package ui.parts;

import graph.MyGraph;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class TimeSelector extends JPanel{

   private JLabel timeLabel;

   public TimeSelector() {
      super(new FlowLayout(FlowLayout.LEFT));
      JSpinner spinner = new JSpinner(new SpinnerNumberModel(15, 1, 1000, 1));
      this.timeLabel = new JLabel("Time requirement for data: ");
      spinner.addChangeListener(new ChangeListener() {
         @Override
         public void stateChanged(ChangeEvent e) {
            JSpinner source = (JSpinner) e.getSource();
            int time = Integer.parseInt(source.getValue().toString());
            MyGraph.setTimeRequirement(time);
         }
      });
      this.add(timeLabel);
      this.add(spinner);
   }

   public int getLabelHeight() {
      return timeLabel.getHeight();
   }
}
