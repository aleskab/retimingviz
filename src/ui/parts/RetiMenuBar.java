package ui.parts;

import ui.MainSplitPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This file is part of retimingViz application and is distributed under BSD license reproduced in file <COPYING>.
 * If file is missing license is reproduced here: <http://opensource.org/licenses/BSD-3-Clause>
 *
 * Created with IntelliJ IDEA.
 * User: Oleg.Bahvalov
 * Date: 3.02.14
 */
public class RetiMenuBar extends JMenuBar {

   private MainSplitPane owner;

   public RetiMenuBar(final MainSplitPane owner) {   //owner is mainsplitpane
      this.owner = owner;

      setLayout(new FlowLayout(FlowLayout.LEFT));
      setBorderPainted(true);

      addModeMenu();
      addRetimeButtons();
      addCenterLocationButton();
      addShowDetailsButton();
   }

   private void addRetimeButtons() {
      final JButton startButton = new JButton("Retime [ > ] ");
      final JButton stepButton = new JButton("Step [ || ] ");

      ActionListener retimeListener = new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(stepButton)) {
               owner.getInitPanel().retimeMyGraph(true);
            } else {
               owner.getInitPanel().retimeMyGraph(false);
            }

            if (owner.getDetailsPanel().isInfoDialogVisible()) {
               owner.getDetailsPanel().updateData();
            }
            startButton.setSelected(false);
         }
      };
      startButton.addActionListener(retimeListener);
      stepButton.addActionListener(retimeListener);
      add("start", startButton);
      add("step", stepButton);
   }

   private void addShowDetailsButton() {
      final JToggleButton showDetailsButton = new JToggleButton("Show option Buttons");
      showDetailsButton.setFont(UIManager.getFont("Button.font"));
      showDetailsButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent event) {
            owner.toggleVisibilityOfDetails();
            if (owner.isDetailsPanelVisible()) {
               showDetailsButton.setText("Hide option buttons");
            } else {
               showDetailsButton.setText("Show option buttons");
            }
         }
      });
      add(showDetailsButton);
   }

   private void addCenterLocationButton() {
      JButton centerLocationButton = new JButton("Center Location");
      centerLocationButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            owner.getInitPanel().centerGraphLocation();
         }
      });
      add(centerLocationButton);
   }

   private void addModeMenu() {
      JMenu modeMenu = owner.getInitPanel().getMyGraphMouseModeMenu(); // Obtain mode menu from the mouse
      modeMenu.setText("Menu");
      modeMenu.setIcon(UIManager.getIcon("FileView.computerIcon"));
      modeMenu.setPreferredSize(new Dimension(80, 20)); // Change the size

      add(modeMenu);
      JComboBox mouseModes = owner.getInitPanel().getMyGraphMouseComboBox();
      mouseModes.setFont(UIManager.getFont("ComboBox.font"));
      add(mouseModes);
   }

}
